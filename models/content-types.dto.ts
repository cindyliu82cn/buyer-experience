/**
 * Generic ContentfulEntry type
 */
export interface CtfEntry<T> {
  fields: T;
  sys: any;
}

export interface CtfPage {
  title: string;
  slug: string;
  description: string;
  seoMetadata: Array<CtfEntry<CtfSeo>>;
  schema: any;
  pageContent: Array<CtfEntry<any>>;
}

/**
 * Contentful generic Image ContentType
 */
export interface CtfImage {
  title: string;
  description: string;
  file: {
    url: string;
  };
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/headerAndText/fields
 */
export interface CtfHeaderAndText {
  header: string;
  text: string;
  headerAnchorId: string;
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/asset/fields
 */
export interface CtfAsset {
  image: CtfEntry<CtfImage>;
  altText: string;
  link: string;
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/button/fields
 */
export interface CtfButton {
  internalName: string;
  text: string;
  externalUrl: string;
  disabled: boolean;
  dataGaName: string;
  dataGaLocation: string;
  iconName: string;
  iconVariant: string;
  openInNewTab: boolean;
  variation: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/externalVideo/fields
 */
export interface CtfExternalVideo {
  internalName: string;
  title: string;
  url: string;
  thumbnail: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/eventHero/fields
 */
export interface CtfHero {
  internalName: string;
  title: string;
  subheader: string;
  componentName: string;
  variant: string;
  description: string;
  backgroundImage: CtfEntry<CtfImage>;
  primaryCta: CtfEntry<CtfButton>;
  secondaryCta: CtfEntry<CtfButton>;
  video: CtfEntry<CtfExternalVideo>;
  crumbs: CtfEntry<CtfButton>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/twoColumnBlock/fields
 */
export interface CtfTwoColumnBlock {
  internalName: string;
  header: string;
  componentId: string;
  componentName: string;
  subheader: string;
  text: string;
  readMoreText: string;
  readMoreControlsText: string;
  assets: CtfEntry<CtfExternalVideo>[] | CtfEntry<CtfAsset>[];
  cta: CtfEntry<CtfButton>;
  secondaryCta: CtfEntry<CtfButton>;
  blockGroup: CtfEntry<any>[];
  customFields: any;
}

export interface CtfCard {
  internalName: string;
  title: string;
  componentName: string;
  subtitle: string;
  description: string;
  list: string[];
  pills: string[];
  variant: string;
  iconName: string;
  iconVariant: string;
  image: CtfEntry<CtfImage>;
  video: CtfEntry<CtfExternalVideo>;
  button: CtfEntry<CtfButton>;
  secondaryButton: CtfEntry<CtfButton>;
  column2Title: string;
  column2Description: string;
  column2List: string[];
  column2Cta: CtfEntry<CtfButton>;
  cardLink: string;
  cardLinkDataGaName: string;
  cardLinkDataGaLocation: string;
  contentArea: CtfEntry<any>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/cardGroup/fields
 */
export interface CtfCardGroup {
  internalName: string;
  header: string;
  componentName: string;
  headerAnchorId: string;
  subheader: string;
  description: string;
  image: CtfEntry<CtfAsset>;
  video: CtfEntry<CtfExternalVideo>;
  cta: CtfEntry<CtfButton>;
  card: CtfEntry<CtfCard>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/blockGroup/fields
 */
export interface CtfBlockGroup {
  internalName: string;
  header: string;
  componentName: string;
  subheader: string;
  description: string;
  blocks:
    | CtfEntry<CtfTwoColumnBlock>[]
    | CtfEntry<CtfCardGroup>
    | CtfEntry<CtfBlockGroup>;
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/anchorLink/fields
 */
export interface CtfAnchorLink {
  internalName: string;
  linkText: string;
  anchorLink: string;
  nodes: any;
  dataGaName: string;
  dataGaLocation: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/sideMenu/fields
 */
export interface CtfSideNavigation {
  internalName: string;
  header: string;
  footer: string;
  anchorsText: string;
  anchors: CtfEntry<CtfAnchorLink>[];
  hyperlinksText: string;
  hyperlinks: CtfEntry<any>[];
  content: CtfEntry<any>[];
  customFields: any;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/faq/fields
 */
export interface CtfFaq {
  id: string;
  title: string;
  subtitle: string;
  dataInboundAnalytics: string;
  background: boolean;
  showAllButton: boolean;
  expandAllCarouselItemsText: string;
  hideAllCarouselItemsText: string;
  jumpDownButtonText: string;
  accordionType: string;
  singleAccordionGroupItems:
    | CtfEntry<CtfCardGroup>[]
    | CtfEntry<CtfHeaderAndText>[];
  multipleAccordionGroups: CtfEntry<CtfFaq>[];
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/tabControl/preview
 */
export interface CtfTabControl {
  internalName: string;
  TabId: string;
  tabButtonText: string;
  tabIconName: string;
  tabButtonLogo: CtfEntry<CtfAsset>;
  tabCta: CtfEntry<CtfButton>;
  tabPanelContent: CtfEntry<any>[];
  tabGaName: string;
  tabGaLocation: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/tabControlsContainer/fields
 */
export interface CtfTabControlsContainer {
  internalName: string;
  componentName: string;
  header: string;
  tabControlsSubtext: string;
  tabs: CtfEntry<any>;
  customFields: any;
}

export interface CtfQuote {
  internalName: string;
  quoteText: string;
  author?: string;
  authorTitle?: string;
  authorCompany?: string;
  authorHeadshot?: CtfEntry<CtfAsset>;
  authorHeadshotCustomClass?: string;
  cta?: CtfEntry<CtfButton>;
  customFields?: any;
}

export interface CtfForm {
  formId: string;
  multiStepForm: boolean;
  componentName: string;
  formDataLayer: string;
  registrationClosed: boolean;
  header: string;
  blurb: string;
  confirmationMessage: string;
  closedRegistrationMessage: string;
  disclaimer: string;
  showDisclaimer: boolean;
  errorMessage: string;
  errorCtas: Array<CtfEntry<CtfButton>>;
  requiredFieldsText: string;
}

/**
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/event/fields
 */
export interface CtfEventPage {
  internalName: string;
  name: string;
  seo: CtfEntry<CtfSeo>;
  slug: string;
  format: string;
  eventType: string;
  description: string;
  date: string;
  endDate: string;
  dateWithTime: string;
  location: string;
  locationState: string;
  region: string;
  eventURL: string;
  eventCardEyebrowImage: CtfEntry<CtfImage>;
  hero: CtfEntry<CtfHero>;
  blurb: string;
  booth: Array<CtfEntry<CtfTwoColumnBlock>>;
  social: Array<CtfEntry<CtfTwoColumnBlock>>;
  agenda: Array<CtfEntry<CtfHeaderAndText>>;
  sessions: Array<CtfEntry<CtfTwoColumnBlock>>;
  staticFields: object;
  form: string;
  careers: Array<CtfEntry<CtfTwoColumnBlock>>;
  footnote: Array<CtfEntry<CtfHeaderAndText>>;
  featuredContent: Array<CtfEntry<any>>;
  nextSteps: CtfEntry<any>;
  showOnLandingPage: boolean;
}

/**
 * https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/content_types/videoCarousel/preview
 */
export interface CtfVideoCarousel {
  header: string;
  videos: Array<CtfEntry<CtfExternalVideo>>;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/textBlock/fields
 */
export interface CtfTextBlock {
  headerAnchorId?: string;
  internalName: string;
  header?: string;
  text?: string;
  componentName: string;
  ctaText?: string;
  ctaUrl?: string;
  image?: CtfEntry<CtfImage>;
  videoUrl?: string;
}

export interface CtfCta {
  headerAnchorId?: string;
  header?: string;
  subheader: string;
  description: string;
  buttonText: string;
  buttonUrl: string;
  iconName: string;
}

export interface CtfSeo {
  title: string;
  description: string;
  noIndex: boolean;
  ogTitle: string;
  ogImage: CtfEntry<CtfAsset>;
  ogImageAlt: string;
  ogUrl: string;
  ogDescription: string;
  ogSiteName: string;
  ogType: string;
}

export interface CtfCardBlockCard {
  header?: string;
  description?: string;
  link?: string;
  icon?: string;
}

export interface CtfCardBlock {
  headerAnchorId?: string;
  header?: string;
  cards: CtfEntry<CtfCardBlockCard>[];
}

export interface CtfLandingGrid {
  columns: number;
  cards: CtfEntry<CtfLandingGridCard>[];
}

export interface CtfLandingGridCard {
  header: string;
  slug: string;
  description?: string;
  date: string;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/customPage/fields
 */
export interface CtfCustomPage {
  pageName: string;
  slug: string;
  activateSideMenu: boolean;
  seoMetaData: CtfEntry<CtfSeo>;
  date: string;
  header: string;
  subheader: string;
  description: string;
  primaryCtaText: string;
  primaryCtaLink: string;
  secondaryCtaText: string;
  secondaryCtaLink: string;
  components: Array<CtfEntry<any>>;
}

/**
 * Entry: https://app.contentful.com/spaces/xz1dnu24egyd/content_types/marketoForm/fields
 */
export interface CtfMarketoForm {
  internalName?: string;
  formId?: string;
  formDataLayer?: string;
  formHeader?: string;
  confirmationMessageTitle?: string;
  confirmationMessage?: string;
  errorMessage?: string;
  requiredFieldsMessage?: string;
}

export interface CtfPricingCard extends CtfCard {
  header?: string;
  label?: string;
  price?: number;
  priceDescription?: string;
  backgroundGradient?: boolean;
  pricingFeatures: Array<CtfEntry<any>>;
  footnote?: Array<CtfEntry<any>>;
}
