**Reviewer Checklist:**
- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues
- [ ] These changes meet a specific OKR or item in our Quarterly Plan.
- [ ] These changes work on both Safari and Chrome.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes comply with [SEO best practices](https://about.gitlab.com/handbook/marketing/inbound-marketing/search-marketing/seo-content-manual/#digital-content-publication-seo-checklist).
- [ ] Changes with links meet the [data attributes requirement](https://about.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#click-tracking) for Google Analytics tracking.
- [ ] Changes with interactive elements that aren't links (video, drop-down menu, form, etc.) sends a [dataLayer event](https://about.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#data-layer-tracking) for Google Analytics tracking.
- [ ] These changes have been documented as expected.

**Build Variables:**
- [ ] Use Contentful Preview API
