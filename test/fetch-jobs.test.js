// We want test / build to fail if something is wrong with the greenhouse API endpoint
describe('Greenhouse Jobs by Departments Fetch Test', () => {
  it('returns an array of departments that is greater than 0', async () => {
    const greenhouseResponse = await fetch(
      'https://boards-api.greenhouse.io/v1/boards/gitlab/departments',
    );
    const greenhouseData = await greenhouseResponse.json();

    expect(greenhouseData.departments.length).toBeGreaterThanOrEqual(1);
  });
});
