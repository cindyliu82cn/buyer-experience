import { CONTENT_TYPES } from '~/common/content-types';
import { getUrlFromContentfulImage } from '~/common/util';
import { COMPONENT_NAMES, ICON_VARIANTS } from '~/common/constants';
import {
  CtfCard,
  CtfCardGroup,
  CtfEntry,
  CtfForm,
  CtfTwoColumnBlock,
} from '~/models';

export function mapReportCta(ctfReportCta: CtfCardGroup) {
  // For a CTA with a single card to be displayed, pass {'singleCta': true} in customFields.
  // This will use the single-cta-block
  if (ctfReportCta.customFields.singleCta) {
    return {
      name: COMPONENT_NAMES.SINGLE_CTA_BLOCK,
      data: {
        cards: ctfReportCta.card.map((card) => {
          return {
            header: card.fields.title,
            icon: card.fields.iconName,
            link: {
              text: card.fields.button.fields.text,
              url: card.fields.button.fields.externalUrl,
              data_ga_name: card.fields.button.fields.dataGaName,
            },
          };
        }),
      },
    };
  }

  return {
    name: COMPONENT_NAMES.REPORT_CTA,
    data: {
      ...ctfReportCta.customFields,
      title: ctfReportCta.header,
      reports: ctfReportCta.card.map((card: any) => ({
        description: card.fields?.description,
        url: card.fields?.button?.fields.externalUrl,
        link_text: card.fields?.button?.fields.text,
        data_ga_name: card.fields?.button?.fields.dataGaName,
        data_ga_location: card.fields?.button?.fields.dataGaLocation,
      })),
    },
  };
}

export function mapDemoCta(ctfDemoCta: any) {
  return {
    name: COMPONENT_NAMES.DEMO_CTA,
    data: {
      ...ctfDemoCta.customFields,
      header: ctfDemoCta.header,
      subheader: ctfDemoCta.subheader,
      demo: {
        subtitle: ctfDemoCta.card[0].fields.subtitle,
        video: {
          url: ctfDemoCta.card[0].fields.video.fields.url,
          thumbnail:
            'https://images.ctfassets.net/xz1dnu24egyd/4GIgTOEntGXvhBZZXnx2ch/39da36485919940fd4a4c7bb34eccf27/Screenshot_2024-01-31_at_4.50.30_PM.png',
          altText: 'A screen shot of a computer screen with a black screen.',
        },
        scheduleButton: {
          text: ctfDemoCta.card[0].fields.button.fields.text,
          href: ctfDemoCta.card[0].fields.button.fields.externalUrl,
          data_ga_name: ctfDemoCta.card[0].fields.button.fields.dataGaName,
          data_ga_location:
            ctfDemoCta.card[0].fields.button.fields.dataGaLocation,
        },
      },
      launchButton: {
        text: ctfDemoCta.card[0].fields.secondaryButton.fields.text,
        href: ctfDemoCta.card[0].fields.secondaryButton.fields.externalUrl,
        icon_name: ctfDemoCta.card[0].fields.secondaryButton.fields.iconName,
        icon_variant:
          ctfDemoCta.card[0].fields.secondaryButton.fields.iconVariant,
        data_ga_name:
          ctfDemoCta.card[0].fields.secondaryButton.fields.dataGaName,
        data_ga_location:
          ctfDemoCta.card[0].fields.secondaryButton.fields.dataGaLocation,
      },
    },
  };
}

export function mapSolutionsHero(ctfHero: any) {
  const primary_btn = ctfHero.primaryCta && {
    text: ctfHero?.primaryCta.fields.text,
    url: ctfHero?.primaryCta.fields.externalUrl,
    icon: ctfHero?.primaryCta.fields.iconName && {
      name: ctfHero.primaryCta.fields.iconName,
      variant: ctfHero.primaryCta.fields.iconVariant || 'product',
    },
    data_ga_name: ctfHero?.primaryCta.fields.dataGaName,
    data_ga_location: ctfHero?.primaryCta.fields.dataGaLocation,
  };

  const secondary_btn = ctfHero?.secondaryCta && {
    text: ctfHero?.secondaryCta.fields?.text,
    url: ctfHero?.secondaryCta.fields?.externalUrl,
    icon: ctfHero?.secondaryCta.fields.iconName && {
      name: ctfHero.secondaryCta.fields.iconName,
      variant: ctfHero.secondaryCta.fields.iconVariant || 'product',
    },
    data_ga_name: ctfHero?.secondaryCta.fields?.dataGaName,
    data_ga_location: ctfHero?.secondaryCta.fields?.dataGaLocation,
  };
  const badge = ctfHero?.eyebrowBadgeButton && {
    text: ctfHero?.eyebrowBadgeButton.fields?.text,
    url: ctfHero?.eyebrowBadgeButton.fields?.externalUrl,
    data_ga_name: ctfHero?.eyebrowBadgeButton.fields?.dataGaName,
    data_ga_location: ctfHero?.eyebrowBadgeButton.fields?.dataGaLocation,
    icon: ctfHero?.eyebrowBadgeButton.fields?.iconName,
  };

  const video = ctfHero.video && {
    video_url: ctfHero.video.fields.url,
  };

  const image = ctfHero.backgroundImage && {
    image_url: ctfHero?.backgroundImage?.fields?.file?.url,
    alt: ctfHero?.backgroundImage?.fields?.title,
    bordered: ctfHero?.customFields?.bordered,
    image_purple_background: ctfHero?.customFields?.image_purple_background,
    is_video_thumbnail: ctfHero?.customFields?.is_video_thumbnail,
  };

  return {
    name: COMPONENT_NAMES.SOLUTIONS_HERO,
    data: {
      ...ctfHero?.customFields,
      title: ctfHero?.title,
      subtitle: ctfHero?.description,
      ...(ctfHero?.subheader ? { note: [ctfHero.subheader] } : {}),
      primary_btn,
      secondary_btn,
      badge,
      video,
      image,
    },
  };
}

export function mapCopyMedia(ctfBlockGroup: any) {
  const formattedBlocks = ctfBlockGroup.blocks.map((block: any) => {
    const formattedBlock: any = {
      ...block.fields?.customFields,
      header: block.fields?.header,
      subtitle: block.fields?.subheader ?? null,
      link_href: block.fields?.cta?.fields.externalUrl,
      link_text: block.fields?.cta?.fields.text,
      link_data_ga_name: block.fields?.cta?.fields.dataGaName,
      link_data_ga_location: block.fields?.cta?.fields.dataGaLocation,
    };

    const asset = block.fields?.assets && block.fields?.assets[0];

    if (asset) {
      if (asset.sys.contentType.sys.id === CONTENT_TYPES.EXTERNAL_VIDEO) {
        formattedBlock.video = { video_url: asset.fields?.url };
      }
      if (asset.sys?.contentType?.sys.id === CONTENT_TYPES.ASSET) {
        formattedBlock.image = {
          ...asset.fields?.customFields,
          alt: asset.fields?.altText,
          image_url: getUrlFromContentfulImage(asset.fields.image),
        };
      }
    }

    if (formattedBlock.miscellaneous) {
      formattedBlock.miscellaneous = block.fields.text;
      formattedBlock.media_link_href =
        block.fields.secondaryCta?.fields?.externalUrl;
      formattedBlock.media_link_text = block.fields.secondaryCta?.fields?.text;
      formattedBlock.media_link_data_ga_name =
        block.fields.secondaryCta?.fields?.dataGaName;
      formattedBlock.media_link_data_ga_location =
        block.fields.secondaryCta?.fields?.dataGaLocation;
    } else {
      formattedBlock.text = block.fields.text;
    }

    return formattedBlock;
  });

  return {
    name: COMPONENT_NAMES.COPY_MEDIA,
    data: {
      ...ctfBlockGroup.customFields,
      header: ctfBlockGroup.header,
      block: formattedBlocks,
    },
  };
}

export function mapCopy(ctfBlockGroup: any) {
  return {
    name: COMPONENT_NAMES.COPY,
    data: {
      block: ctfBlockGroup.blocks.map((block: any) => ({
        ...block.fields.customFields,
        header: block.fields.header,
        text: block.fields.text,
      })),
    },
  };
}

export function mapVideoCarousel(ctfCarousel: any) {
  const videos = ctfCarousel.videos.map((video: any) => ({
    title: video.fields?.title,
    video_link: video.fields?.url,
    photourl: getUrlFromContentfulImage(video.fields?.thumbnail.fields.image),
    carousel_identifier: [''],
  }));

  return {
    name: COMPONENT_NAMES.VIDEOS_CAROUSEL,
    data: {
      title: ctfCarousel.header,
      videos,
    },
  };
}

export function mapBenefits(ctfBenefits: any) {
  const benefits = ctfBenefits.card.map((card: any) => ({
    title: card.fields?.title,
    description: card.fields?.description,
    icon: {
      name: card.fields?.iconName,
      variant: 'marketing',
    },
  }));

  return {
    name: COMPONENT_NAMES.BENEFITS,
    data: {
      ...ctfBenefits.customFields,
      benefits,
    },
  };
}

export function mapQuote(ctfQuote: any) {
  return {
    name: COMPONENT_NAMES.PULL_QUOTE,
    data: {
      quote: ctfQuote.quoteText,
      source: ctfQuote.author,
      link_href: ctfQuote.cta?.fields.externalUrl,
      link_text: ctfQuote.cta?.fields.text,
      data_ga_name: ctfQuote.cta?.fields.dataGaName,
      data_ga_location: ctfQuote.cta?.fields.dataGaLocation,
    },
  };
}

export function mapFeaturedMedia(ctfCardGroup: CtfCardGroup) {
  const media = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;
    const image = fields.image && {
      url: getUrlFromContentfulImage(fields.image),
      alt: fields.image?.fields?.description,
    };

    const link = fields.button && {
      text: fields.button.fields?.text,
      href: fields.button.fields?.externalUrl,
      data_ga_name: fields.button.fields?.dataGaName,
      data_ga_location: fields.button.fields?.dataGaLocation,
    };

    const video = fields.video && {
      url: fields.video.fields.url,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      text: fields.description,
      image,
      link,
      video,
    };
  });

  return {
    name: COMPONENT_NAMES.FEATURED_MEDIA,
    data: {
      ...ctfCardGroup.customFields,
      header: ctfCardGroup.header,
      description: ctfCardGroup.description,
      media,
    },
  };
}

export function mapLogoLinks(ctfLogos: any) {
  const logos = ctfLogos.logo.map((logo: any) => ({
    ...logo.fields?.customFields,
    logo_url: getUrlFromContentfulImage(logo.fields?.image),
    logo_alt: logo.fields?.image.fields.altText,
  }));

  return {
    name: COMPONENT_NAMES.LOGO_LINKS,
    data: {
      ...ctfLogos.logoGridConfiguration,
      header: ctfLogos.text,
      logos,
    },
  };
}

export function mapTierBlock(ctfCardGroup: any) {
  const tiers = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;

    const cta = fields.button && {
      href: fields.button.fields.externalUrl,
      text: fields.button.fields.text,
      data_ga_name: fields.button.fields.dataGaName,
      data_ga_location: fields.button.fields.dataGaLocation,
    };

    const link = fields.secondaryButton && {
      href: fields.secondaryButton.fields.externalUrl,
      text: fields.secondaryButton.fields.text,
      data_ga_name: fields.secondaryButton.fields.dataGaName,
      data_ga_location: fields.secondaryButton.fields.dataGaLocation,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      subtitle: fields.subtitle || null,
      items: fields.list,
      cta,
      link,
    };
  });

  const cta = ctfCardGroup.cta && {
    url: ctfCardGroup.cta.fields.externalUrl,
    text: ctfCardGroup.cta.fields.text,
    data_ga_name: ctfCardGroup.cta.fields.dataGaName,
    data_ga_location: ctfCardGroup.cta.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.TIER_BLOCK,
    data: {
      ...ctfCardGroup.customFields,
      header: ctfCardGroup.header,
      cta,
      tiers,
    },
  };
}

export function mapEducationCaseStudyCarousel(ctfCaseStudyCarousel: any) {
  const caseStudies =
    ctfCaseStudyCarousel.tabs &&
    ctfCaseStudyCarousel.tabs?.map((tab) => {
      return {
        logo_url: tab.fields.tabButtonLogo.fields.image.fields.file.url,
        instituion_name: tab.fields.tabId,
        img_url:
          tab.fields.tabPanelContent[0].fields.authorHeadshot.fields.image
            .fields.file.url,
        quote: {
          quote_text: tab.fields.tabPanelContent[0].fields.quoteText,
          author: tab.fields.tabPanelContent[0].fields.author,
          author_title: tab.fields.tabPanelContent[0].fields.authorTitle,
        },
        case_study_url: tab.fields.tabCta.fields.externalUrl,
        data_ga_name: tab.fields.tabCta.dataGaName,
        data_ga_location: tab.fields.tabCta.dataGaLocation,
      };
    });

  const caseStudiesCards =
    ctfCaseStudyCarousel.card &&
    ctfCaseStudyCarousel.card?.map((card) => {
      return {
        logo_url: card.fields.image?.fields.file.url,
        instituion_name: card.fields.contentArea[0]?.fields.authorCompany,
        quote: {
          img_url:
            card.fields.contentArea[0].fields.authorHeadshot?.fields.image
              .fields.file.url,
          quote_text: card.fields.contentArea[0]?.fields.quoteText,
          author: card.fields.contentArea[0]?.fields.author,
          author_title: card.fields.contentArea[0]?.fields.authorTitle,
        },
        case_study_url: card.fields?.cardLink,
        data_ga_name: card.fields?.cardLinkDataGaName,
        data_ga_location: card.fields?.cardLinkDataGaLocation,
      };
    });

  const cta = ctfCaseStudyCarousel.cta
    ? {
        href: ctfCaseStudyCarousel.cta.fields.externalUrl,
        text: ctfCaseStudyCarousel.cta.fields.text,
        data_ga_name: ctfCaseStudyCarousel.cta.fields.dataGaName,
      }
    : null;

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL,
    data: {
      header: ctfCaseStudyCarousel.header || null,
      cta,
      case_studies: caseStudies || caseStudiesCards,
      ...ctfCaseStudyCarousel.customFields,
    },
  };
}

export function mapEducationCaseStudyTabs(ctfCaseStudyTabs: any) {
  const caseStudies = ctfCaseStudyTabs.tabs.map((tab) => {
    return {
      tab: tab.fields.tabButtonText,
      title: tab.fields.tabPanelContent[0].fields.header,
      text: tab.fields.tabPanelContent[0].fields.description,
      cta: {
        text: tab.fields.tabCta.fields.text,
        url: tab.fields.tabCta.fields.externalUrl,
        data_ga_name: tab.fields.tabCta.fields.dataGaName,
        data_ga_location: tab.fields.tabCta.fields.dataGaLocation,
      },
      cards: tab.fields.tabPanelContent[0].fields.card.map((card: any) => ({
        title: card.fields.title,
        text: card.fields.description,
        icon_name: card.fields.iconName,
        cta: {
          text: card.fields.button.fields.text,
          url: card.fields.button.fields.externalUrl,
          data_ga_name: card.fields.button.fields.dataGaName,
          data_ga_location: card.fields.button.fields.dataGaLocation,
        },
      })),
    };
  });

  return {
    name: ctfCaseStudyTabs.componentName,
    data: {
      heading: ctfCaseStudyTabs.header,
      tabs: caseStudies,
    },
  };
}

export function mapEducationTiers(ctfTiers: any) {
  const teacherFeatures = ctfTiers.card[0].fields.list.map((feature) => {
    return {
      item: feature,
    };
  });

  const schoolFeatures = ctfTiers.card[1].fields.list.map((feature) => {
    return {
      item: feature,
    };
  });

  return {
    name: 'education-feature-tiers',
    data: {
      heading: ctfTiers.header,
      teacher_tier: {
        title: ctfTiers.card[0].fields.title,
        text: ctfTiers.card[0].fields.subtitle,
        disclaimer: ctfTiers.card[0].fields.description,
        features: teacherFeatures,
        cta: {
          text: ctfTiers.card[0].fields.button.fields.text,
          url: ctfTiers.card[0].fields.button.fields.externalUrl,
          data_ga_name: ctfTiers.card[0].fields.button.fields.dataGaName,
          data_ga_location:
            ctfTiers.card[0].fields.button.fields.dataGaLocation,
        },
      },
      schools_tier: {
        title: ctfTiers.card[1].fields.title,
        text: ctfTiers.card[1].fields.subtitle,
        disclaimer: ctfTiers.card[1].fields.description,
        features: schoolFeatures,
        cta: {
          text: ctfTiers.card[1].fields.button.fields.text,
          url: ctfTiers.card[1].fields.button.fields.externalUrl,
          data_ga_name: ctfTiers.card[1].fields.button.fields.dataGaName,
          data_ga_location:
            ctfTiers.card[1].fields.button.fields.dataGaLocation,
        },
        callout_box: {
          title: ctfTiers.card[1].fields.column2Title,
          cta: {
            text: ctfTiers.card[1].fields.secondaryButton.fields.text,
            url: ctfTiers.card[1].fields.secondaryButton.fields.externalUrl,
            data_ga_name:
              ctfTiers.card[1].fields.secondaryButton.fields.dataGaName,
            data_ga_location:
              ctfTiers.card[1].fields.secondaryButton.fields.dataGaLocation,
          },
        },
      },
    },
  };
}

export function mapSolutionsCtaCard(ctfCard: CtfCard) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_CTA_CARD,
    data: {
      ...ctfCard?.customFields,
      title: ctfCard?.title,
      description: ctfCard?.description,
      button: {
        text: ctfCard?.button?.fields?.text,
        href: ctfCard?.cardLink || ctfCard.button?.fields.externalUrl,
        data_ga_name: ctfCard?.button?.fields?.dataGaName,
        data_ga_location: 'body',
      },
    },
  };
}

export function mapEducationStats(ctfStats: any) {
  const statsArray = ctfStats.card.map((stat) => {
    return {
      spotlight_text: stat.fields.title,
      text: stat.fields.subtitle,
    };
  });

  return {
    name: 'education-stats',
    data: {
      header: ctfStats.header,
      stats: statsArray,
    },
  };
}

export function mapPricingTiers(ctfCardGroup: any) {
  return {
    name: COMPONENT_NAMES.PRICING_TIERS,
    data: {
      ...ctfCardGroup?.customFields,
      title: ctfCardGroup?.header,
      subtitle: ctfCardGroup?.fields?.cta?.fields?.text,
      description: ctfCardGroup?.description,
      cards: ctfCardGroup?.card,
    },
  };
}

export function mapSolutionsResourceCards(ctfCardGroup: CtfCardGroup) {
  const cards = ctfCardGroup.card.map((card: CtfEntry<CtfCard>) => ({
    ...card.fields?.customFields,
    header: card.fields?.title,
    link_text: card.fields?.button?.fields.text,
    image: getUrlFromContentfulImage(card.fields?.image),
    href: card.fields?.button?.fields.externalUrl,
    data_ga_name: card.fields?.button?.fields.dataGaName,
    data_ga_location: card.fields?.button?.fields.dataGaLocation,
    icon: card.fields?.iconName && {
      name: card.fields?.iconName,
      variant: ICON_VARIANTS.MARKETING,
    },
  }));

  return {
    name: COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      header_cta_text: ctfCardGroup.cta?.fields.text,
      header_cta_href: ctfCardGroup.cta?.fields.externalUrl,
      header_cta_ga_name: ctfCardGroup.cta?.fields.dataGaName,
      header_cta_ga_location: ctfCardGroup.cta?.fields.dataGaLocation,
      cards,
    },
  };
}

export function mapResourceCarousel(ctfCardGroup: CtfCardGroup) {
  const cards = ctfCardGroup.card.map((card: CtfEntry<CtfCard>) => ({
    ...card.fields?.customFields,
    header: card.fields?.title,
    link_text: card.fields?.button?.fields.text,
    image: getUrlFromContentfulImage(card.fields?.image),
    href: card.fields?.button?.fields.externalUrl,
    data_ga_name: card.fields?.button?.fields.dataGaName,
    data_ga_location: card.fields?.button?.fields.dataGaLocation,
  }));

  return {
    name: COMPONENT_NAMES.RESOURCE_CAROUSEL,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      header_cta_text: ctfCardGroup.cta?.fields.text,
      header_cta_href: ctfCardGroup.cta?.fields.externalUrl,
      header_cta_ga_name: ctfCardGroup.cta?.fields.dataGaName,
      header_cta_ga_location: ctfCardGroup.cta?.fields.dataGaLocation,
      cards,
    },
  };
}

export function mapSolutionsCards(ctfCardGroup: any) {
  const cards = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description,
    cta: card.fields?.button?.fields.text,
    href: card.fields?.button?.fields.externalUrl,
    data_ga_name: card.fields?.button?.fields.dataGaName,
    data_ga_location: card.fields?.button?.fields.dataGaLocation,
  }));

  const link = ctfCardGroup.cta && {
    url: ctfCardGroup.cta?.fields?.externalUrl,
    text: ctfCardGroup.cta?.fields?.text,
    data_ga_name: ctfCardGroup.cta?.fields.dataGaName,
    data_ga_location: ctfCardGroup.cta?.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.SOLUTIONS_CARDS,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      link,
      cards,
    },
  };
}

export function mapGroupButtons(ctfCardGroup: any) {
  const link = ctfCardGroup.cta && {
    text: ctfCardGroup.cta.fields.text,
    href: ctfCardGroup.cta.fields.externalUrl,
  };

  const buttons = ctfCardGroup.card.map((card: any) => ({
    text: card.fields?.title,
    icon_left: card.fields?.iconName,
    href: card.fields?.cardLink,
  }));

  return {
    name: COMPONENT_NAMES.GROUP_BUTTONS,
    data: {
      ...ctfCardGroup.customFields,
      header: {
        text: ctfCardGroup.header,
        link,
      },
      buttons,
    },
  };
}

export function mapSolutionsFeatureList(ctfCardGroup: any) {
  const features = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description,
    image_tagline: card.fields?.subtitle,
    image_url: getUrlFromContentfulImage(card.fields?.image),
    image_alt: card.fields?.image.fields.description,
  }));

  const icon = ctfCardGroup.image && {
    name: getUrlFromContentfulImage(ctfCardGroup.image.fields.image),
    alt: ctfCardGroup.image.fields.altText,
  };

  return {
    name: COMPONENT_NAMES.SOLUTIONS_FEATURE_LIST,
    data: {
      title: ctfCardGroup.header,
      subtitle: ctfCardGroup.description,
      icon,
      features,
    },
  };
}

export function mapSolutionsVideo(ctfVideo: any) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_VIDEO_FEATURE,
    data: {
      video: {
        url: ctfVideo.url,
      },
    },
  };
}

export function mapNewQuotesCarousel(ctfQuotesCarousel: any) {
  return {
    name: COMPONENT_NAMES.NEW_QUOTES_CAROUSEL,
    'data-darkmode': ctfQuotesCarousel?.customFields?.['data-darkmode'],
    data: {
      ...ctfQuotesCarousel.customFields,
      title: ctfQuotesCarousel.header,
      rows: ctfQuotesCarousel.card.map((card: any) => ({
        title: card?.fields?.title,
        subtitle: card?.fields?.subtitle,
        description: card?.fields?.description,
        image: {
          alt: card?.fields?.image?.fields?.title,
          url: card?.fields?.image?.fields?.file?.url,
        },
        button: {
          text: `${card?.fields?.title}`,
          href: card?.fields?.button?.fields?.externalUrl,
          data_ga_name: `${card?.fields?.title}`,
          data_ga_location: 'body',
        },
      })),
    },
  };
}

export function mapFormSection(ctfFormSection: CtfTwoColumnBlock) {
  const { header, text, blockGroup, componentId } = ctfFormSection;
  const form: CtfForm | undefined = blockGroup[0]?.fields;

  const mappedBlocks = blockGroup.slice(1).map((block) => {
    const { header, text } = block.fields;
    if (header) {
      return {
        title: header,
        content: text,
      };
    }

    return {
      content: text,
    };
  });

  if (blockGroup.length > 2) {
    return {
      name: COMPONENT_NAMES.OPEN_SOURCE_FORM_SECTION,
      data: {
        id: COMPONENT_NAMES.APPLICATION_FORM,
        title: header,
        blocks: [{ content: text }, ...mappedBlocks],
        form: {
          formId: form.formId,
          form_header: '',
          datalayer: form.formDataLayer,
        },
      },
    };
  }

  return {
    name: COMPONENT_NAMES.OPEN_SOURCE_FORM_SECTION,
    data: {
      id: componentId,
      title: header,
      blocks: [{ content: text }],
      form: form && {
        formId: form.formId,
        form_header: '',
        datalayer: form.formDataLayer,
      },
    },
  };
}

export function mapQuotesCarousel(ctfCardGroup: CtfCardGroup) {
  const { header, card: cards, customFields } = ctfCardGroup;

  return {
    name: COMPONENT_NAMES.QUOTES_CAROUSEL,
    data: {
      header,
      case_studies: cards.map((card) => {
        return {
          logo_url: `${card.fields.image.fields.file.url}?q=80`,
          instituion_name: card.fields.contentArea[0].fields.authorCompany,
          quote: {
            quote_text: card.fields.contentArea[0].fields.quoteText,
            author: card.fields.contentArea[0].fields.author,
            author_title: card.fields.contentArea[0].fields.authorTitle,
          },
        };
      }),
      ...customFields,
    },
  };
}
