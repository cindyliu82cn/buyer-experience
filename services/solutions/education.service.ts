import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '../../common/content-types';
import { getClient } from '../../plugins/contentful.js';
import { COMPONENT_NAMES, TEMPLATES } from '../../common/constants';
import { getUrlFromContentfulImage } from '../../common/util';
import {
  mapCopy,
  mapCopyMedia,
  mapEducationCaseStudyCarousel,
  mapEducationCaseStudyTabs,
  mapEducationStats,
  mapEducationTiers,
  mapFeaturedMedia,
  mapQuote,
  mapSolutionsHero,
} from './solutions-default.helper';
import { mapByIndustryQuotesCarousel } from './solutions-by-industry.helper';
import {
  mapBySolutionIntro,
  mapBySolutionLink,
  mapBySolutionList,
} from './solutions-by-solution.helper';

export class SolutionsEducationService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns Solutions content for the pages
   * @param slug
   * @param isIndexSlug
   */
  getContent(slug: string, isIndexSlug: boolean) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      return this.getContentfulData(slug);
    }

    // Localized data - YML files
    return this.$ctx
      .$content(this.$ctx.i18n.locale, `${slug}${isIndexSlug ? '/index' : ''}`)
      .fetch();
  }

  private async getContentfulData(slug: string) {
    const solutionEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 10,
    });

    if (solutionEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [solutions] = solutionEntry.items;

    return this.transformContentfulData(solutions);
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const mappedContent = this.mapPageContent(pageContent, schema?.template);
    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    return {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      ...mappedContent,
    };
  }

  private mapPageContent(pageContent: any[], template: string) {
    const ctfComponents = pageContent.filter(
      (item) =>
        item.fields.componentName !== 'report-cta' ||
        template === TEMPLATES.Education,
    );

    const components = ctfComponents
      .map((component) => this.getComponentFromEntry(component))
      .filter((component) => component);

    return { components };
  }

  /**
   * Main method that switches which components should be used depending on the Content Type and Component Name
   * @param ctfEntry
   */
  private getComponentFromEntry(ctfEntry: any) {
    let component;

    switch (ctfEntry.sys.contentType.sys.id) {
      case CONTENT_TYPES.HERO: {
        component = mapSolutionsHero(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.BLOCK_GROUP: {
        component = this.mapBlockGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.TAB_CONTROLS_CONTAINER: {
        component = this.mapTabsComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.QUOTE: {
        component = mapQuote(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapHeaderTextComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.FAQ: {
        component = this.mapFaq(ctfEntry);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD: {
        component = this.mapCardComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CUSTOM_COMPONENT: {
        component = { ...ctfEntry.fields };
        break;
      }
    }

    return component;
  }

  private mapBlockGroupComponent(ctfBlockGroup: any) {
    let component;

    switch (ctfBlockGroup.componentName) {
      case COMPONENT_NAMES.COPY_MEDIA: {
        component = mapCopyMedia(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.COPY: {
        component = mapCopy(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL: {
        component = mapByIndustryQuotesCarousel(ctfBlockGroup);
        break;
      }
    }

    return component;
  }

  private mapCardGroupComponent(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.FEATURED_MEDIA: {
        component = mapFeaturedMedia(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_TIERS: {
        component = mapEducationTiers(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_STATS: {
        component = mapEducationStats(ctfCardGroup);
        break;
      }
    }
    return component;
  }

  private mapCardComponent(ctfCard: any) {
    let component;
    switch (ctfCard.componentName) {
      case COMPONENT_NAMES.BY_SOLUTION_INTRO: {
        component = mapBySolutionIntro(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LINK: {
        component = mapBySolutionLink(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LIST: {
        component = mapBySolutionList(ctfCard);
        break;
      }
    }

    return component;
  }

  private mapHeaderTextComponent(ctfHeaderAndText: any) {
    let component;
    switch (ctfHeaderAndText.componentName) {
      case COMPONENT_NAMES.COPY_FORM: {
        component = this.mapCopyForm(ctfHeaderAndText);
        break;
      }
    }
    return component;
  }

  private mapTabsComponent(ctfTabs: any) {
    let component;
    switch (ctfTabs.componentName) {
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL: {
        component = mapEducationCaseStudyCarousel(ctfTabs);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_TABS: {
        component = mapEducationCaseStudyTabs(ctfTabs);
        break;
      }
    }

    return component;
  }

  private mapCopyForm(ctfHeaderAndText: any) {
    return {
      name: ctfHeaderAndText.componentName,
      data: {
        ...ctfHeaderAndText.customFields,
        header: ctfHeaderAndText.header,
        metadata: {
          id_tag: ctfHeaderAndText.headerAnchorId,
        },
        content: ctfHeaderAndText.text,
      },
    };
  }

  private mapFaq(ctfFaq: any) {
    const {
      title,
      singleAccordionGroupItems,
      expandAllCarouselItemsText,
      background,
    } = ctfFaq.fields;

    if (singleAccordionGroupItems) {
      const questions = singleAccordionGroupItems.map((element: any) => ({
        question: element.fields.header,
        answer: element.fields.text,
      }));

      return {
        name: 'faq',
        data: {
          header: title,
          show_all_button: expandAllCarouselItemsText,
          background,
          groups: [{ header: '', questions }],
        },
      };
    }
  }

  private mapSideNavigation(ctfSideMenu: any) {
    const linksArray = ctfSideMenu.anchors
      .map((anchor: any) => anchor.fields)
      .map((anchor: any) => ({
        title: anchor.linkText,
        href: anchor.anchorLink,
      }));

    return {
      ...ctfSideMenu.customFields,
      name: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
      links: linksArray,
      slot_enabled: true,
      slot_content: ctfSideMenu.content.map((item) =>
        this.getComponentFromEntry(item),
      ),
    };
  }
}
