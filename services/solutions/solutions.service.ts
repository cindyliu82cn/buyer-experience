import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';
import { COMPONENT_NAMES, TEMPLATES } from '~/common/constants';
import { getUrlFromContentfulImage } from '~/common/util';
import { CtfTwoColumnBlock } from '~/models/content-types.dto';
import {
  mapBenefits,
  mapCopy,
  mapCopyMedia,
  mapDemoCta,
  mapEducationCaseStudyCarousel,
  mapFeaturedMedia,
  mapFormSection,
  mapGroupButtons,
  mapLogoLinks,
  mapNewQuotesCarousel,
  mapPricingTiers,
  mapQuote,
  mapQuotesCarousel,
  mapReportCta,
  mapResourceCarousel,
  mapSolutionsCards,
  mapSolutionsCtaCard,
  mapSolutionsFeatureList,
  mapSolutionsHero,
  mapSolutionsResourceCards,
  mapSolutionsVideo,
  mapTierBlock,
  mapVideoCarousel,
} from './solutions-default.helper';
import {
  mapByIndustryCaseStudies,
  mapByIndustryQuotesCarousel,
  mapByIndustrySolutionsBlock,
  mapDemoCtaCard,
  mapFaq,
  mapHeaderText,
  mapHomeSolutions,
  mapIndustryIntro,
  mapNonprofitIntro,
  mapNonprofitOverview,
  mapOpenSourceBlog,
  mapOpenSourceFormSection,
  mapOpenSourceOverview,
  mapOpenSourcePartnersGrid,
  mapOpenSourcePartnersGridFull,
  mapStartupsIntro,
  mapStartupsLink,
  mapStartupsOverview,
} from './solutions-by-industry.helper';
import {
  mapBySolutionBenefits,
  mapBySolutionIntro,
  mapBySolutionLink,
  mapBySolutionList,
  mapBySolutionShowcase,
  mapBySolutionValueProp,
} from './solutions-by-solution.helper';

export class SolutionsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns Solutions content for the pages
   * @param slug
   * @param isIndexSlug
   */
  async getContent(slug: string, isIndexSlug: boolean) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      return await this.getContentfulData(slug);
    }

    // PATCH: Due to the complexity of our solutions pages and Nuxt's limitation on nested routes
    // this is a temporary patch for the /startups page
    // that can be removed once i18n content is in the CMS.
    if (slug == 'index') {
      isIndexSlug = true;
    }

    // Localized data - YML files
    const solutions = await this.$ctx
      .$content(this.$ctx.i18n.locale, `${slug}${isIndexSlug ? '/index' : ''}`)
      .fetch();

    // PATCH: Due to the complexity of our solutions pages and Nuxt's limitation on nested routes
    // this is a temporary patch for the /startups page
    // that can be removed once i18n content is in the CMS.
    if (solutions[0]?.dir.includes('/solutions/startups')) {
      return { solutions: solutions[0] };
    }

    return { solutions };
  }

  private async getContentfulData(slug: string) {
    const solutionEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 4,
    });

    if (solutionEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [items] = solutionEntry.items;

    return { items, solutions: this.transformContentfulData(items) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const mappedContent = this.mapPageContent(pageContent, schema?.template);
    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    return {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      ...mappedContent,
    };
  }

  private mapPageContent(pageContent: any[], template: string) {
    const ctfReport = pageContent.find(
      (item) =>
        item.fields.componentName === 'report-cta' &&
        template !== TEMPLATES.Industry,
    );

    const ctfComponents = pageContent.filter(
      (item) =>
        item.fields.componentName !== 'report-cta' ||
        template === TEMPLATES.Industry,
    );

    const components = ctfComponents
      .map((component) => this.getComponentFromEntry(component))
      .filter((component) => component);

    const report_cta: any = ctfReport ? mapReportCta(ctfReport.fields) : {};

    return { components, report_cta: report_cta.data };
  }

  /**
   * Main method that switches which components should be used depending on the Content Type and Component Name
   * @param ctfEntry
   */
  private getComponentFromEntry(ctfEntry: any) {
    let component;

    switch (ctfEntry.sys.contentType.sys.id) {
      case CONTENT_TYPES.HERO: {
        component = mapSolutionsHero(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.BLOCK_GROUP: {
        component = this.mapBlockGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.VIDEO_CAROUSEL: {
        component = mapVideoCarousel(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.QUOTE: {
        component = mapQuote(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CUSTOMER_LOGOS: {
        component = this.mapCustomerLogoComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CARD: {
        component = this.mapCardComponent(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.TWO_COLUMN_BLOCK:
        component = this.mapTwoColumnBlock(ctfEntry.fields);
        break;
      case CONTENT_TYPES.EXTERNAL_VIDEO: {
        component = mapSolutionsVideo(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapHeaderAndText(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.FAQ: {
        component = mapFaq(ctfEntry.fields);
        break;
      }
      case CONTENT_TYPES.CUSTOM_COMPONENT: {
        component = { ...ctfEntry.fields };
        break;
      }

      default:
        component = null;
        break;
    }

    return component;
  }

  private mapCustomerLogoComponent(ctfCustomerLogo: any) {
    let component;

    switch (ctfCustomerLogo.componentName) {
      case COMPONENT_NAMES.LOGO_LINKS: {
        component = mapLogoLinks(ctfCustomerLogo);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_INTRO: {
        component = mapIndustryIntro(ctfCustomerLogo);
        break;
      }
    }

    return component;
  }

  private mapBlockGroupComponent(ctfBlockGroup: any) {
    let component;

    switch (ctfBlockGroup.componentName) {
      case COMPONENT_NAMES.COPY_MEDIA: {
        component = mapCopyMedia(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.COPY: {
        component = mapCopy(ctfBlockGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL: {
        component = mapByIndustryQuotesCarousel(ctfBlockGroup);
        break;
      }
    }

    return component;
  }

  private mapCardGroupComponent(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.BENEFITS: {
        component = mapBenefits(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.FEATURED_MEDIA: {
        component = mapFeaturedMedia(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL:
        component = mapEducationCaseStudyCarousel(ctfCardGroup);
        break;
      case COMPONENT_NAMES.BY_SOLUTION_BENEFITS: {
        component = mapBySolutionBenefits(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_SOLUTIONS_BLOCK: {
        component = mapByIndustrySolutionsBlock(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.NEW_QUOTES_CAROUSEL: {
        component = mapNewQuotesCarousel(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.TIER_BLOCK: {
        component = mapTierBlock(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.PRICING_TIERS: {
        component = mapPricingTiers(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS: {
        component = mapSolutionsResourceCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.RESOURCE_CAROUSEL: {
        component = mapResourceCarousel(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_CARDS: {
        component = mapSolutionsCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP: {
        component = mapBySolutionValueProp(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_INDUSTRY_CASE_STUDIES: {
        component = mapByIndustryCaseStudies(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.HOME_SOLUTIONS_CONTAINER: {
        component = mapHomeSolutions(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.GROUP_BUTTONS: {
        component = mapGroupButtons(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.REPORT_CTA: {
        component = mapReportCta(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.OVERVIEW_BLOCKS: {
        component = mapOpenSourceOverview(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.OPEN_SOURCE_BLOG:
        component = mapOpenSourceBlog(ctfCardGroup);
        break;
      case COMPONENT_NAMES.BY_SOLUTION_SHOWCASE: {
        component = mapBySolutionShowcase(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_FEATURE_LIST: {
        component = mapSolutionsFeatureList(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.STARTUPS_OVERVIEW:
        component = mapStartupsOverview(ctfCardGroup);
        break;
      case COMPONENT_NAMES.NONPROFIT_OVERVIEW:
        component = mapNonprofitOverview(ctfCardGroup);
        break;
      case COMPONENT_NAMES.DEMO_CTA: {
        component = mapDemoCta(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.QUOTES_CAROUSEL: {
        component = mapQuotesCarousel(ctfCardGroup);
        break;
      }
    }
    return component;
  }

  private mapCardComponent(ctfCard: any) {
    let component;
    switch (ctfCard.componentName) {
      case COMPONENT_NAMES.BY_SOLUTION_INTRO: {
        component = mapBySolutionIntro(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LINK: {
        component = mapBySolutionLink(ctfCard);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_LIST: {
        component = mapBySolutionList(ctfCard);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_CTA_CARD: {
        component = mapSolutionsCtaCard(ctfCard);
        break;
      }
      case COMPONENT_NAMES.STARTUPS_LINK: {
        component = mapStartupsLink(ctfCard);
        break;
      }
      case COMPONENT_NAMES.DEMO_CTA_CARD: {
        component = mapDemoCtaCard(ctfCard);
        break;
      }
    }

    return component;
  }

  private mapTwoColumnBlock(ctfTwoColumnBlock: CtfTwoColumnBlock) {
    let component;

    switch (ctfTwoColumnBlock.componentName) {
      case COMPONENT_NAMES.APPLICATION_FORM:
        component = mapFormSection(ctfTwoColumnBlock);
        break;

      case COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID:
        component = mapOpenSourcePartnersGrid(ctfTwoColumnBlock);
        break;

      case COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID_FULL:
        component = mapOpenSourcePartnersGridFull(ctfTwoColumnBlock);
        break;

      case COMPONENT_NAMES.OPEN_SOURCE_FORM_SECTION:
        component = mapOpenSourceFormSection(ctfTwoColumnBlock);
        break;

      default:
        component = mapOpenSourcePartnersGrid(ctfTwoColumnBlock);
    }
    return component;
  }

  private mapSideNavigation(ctfSideMenu: any) {
    const links = ctfSideMenu.anchors
      .map((anchor: any) => anchor.fields)
      .map((anchor: any) => ({
        title: anchor.linkText,
        href: anchor.anchorLink,
        data_ga_name: anchor.dataGaName,
        data_ga_location: anchor.dataGaLocation,
      }));

    return {
      ...ctfSideMenu.customFields,
      name: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
      links,
      slot_content: ctfSideMenu.content.map((item) =>
        this.getComponentFromEntry(item),
      ),
    };
  }

  private mapHeaderAndText(ctfHeaderAndText: any) {
    let component;

    switch (ctfHeaderAndText.componentName) {
      case 'HeaderAndText':
        component = mapHeaderText(ctfHeaderAndText);
        break;
      case COMPONENT_NAMES.STARTUPS_INTRO:
        component = mapStartupsIntro(ctfHeaderAndText);
        break;

      case COMPONENT_NAMES.NONPROFIT_INTRO:
        component = mapNonprofitIntro(ctfHeaderAndText);
        break;

      default:
        component = mapStartupsIntro(ctfHeaderAndText);
    }
    return component;
  }
}
