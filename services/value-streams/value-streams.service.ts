import { metaDataHelper } from '~/services/events/events.helpers';

interface ValueStreamsData {
  title: any;
  og_title?: any;
  description?: any;
  twitter_description?: any;
  og_description?: any;
  components?: any;
  nextSteps?: any;
}

function buttonBuilder(buttonRaw) {
  return {
    text: buttonRaw.text,
    url: buttonRaw.externalUrl,
    data_ga_name: buttonRaw.dataGaName,
    data_ga_location: buttonRaw.dataGaLocation,
    icon: { name: buttonRaw.iconName, variant: buttonRaw.iconVariant },
  };
}
function imageBuilder(imageRaw, customFields) {
  return {
    image_url: imageRaw.file?.url,
    image_url_mobile: imageRaw.file?.url,
    alt: imageRaw.title,
    bordered: customFields?.bordered,
    image_purple_background: customFields?.image_purple_background,
  };
}
function solutionsHeroBuilder(heroRaw) {
  return {
    title: heroRaw.title,
    subtitle: heroRaw.subheader,
    body_note: heroRaw.description,
    rounded_image: heroRaw.customFields?.rounded_image,
    primary_btn: { ...buttonBuilder(heroRaw.primaryCta.fields) },
    image: {
      ...imageBuilder(heroRaw.backgroundImage.fields, heroRaw.customFields),
    },
  };
}
function resourcesBuilder(resourcesArray) {
  return resourcesArray.map((resource) => {
    return {
      text: resource.fields.text,
      icon: {
        name: resource.fields.iconName,
        variant: resource.fields.iconVariant,
      },
    };
  });
}
function securityResourcesBuilder(sectionRaw) {
  return {
    copy: {
      header: { text: sectionRaw.header },
      column_size: 12,
      description: { text: sectionRaw.text },
    },
    resources: {
      column_size: 5,
      items: resourcesBuilder(sectionRaw.blockGroup),
    },
  };
}
function quoteCarouselBuilder(quotesRaw) {
  const quoteFields = quotesRaw.blocks[0]?.fields;
  return {
    header: quotesRaw.header,
    quotes: [
      {
        main_img: quoteFields.authorHeadshot && {
          url: quoteFields.authorHeadshot?.fields.image.fields.file.url,
          alt: quoteFields.authorHeadshot?.fields.altText,
          contentful: true,
        },

        quote: quoteFields.quoteText,
        author: quoteFields.author,
        width: quoteFields.customFields.width,
        height: quoteFields.customFields.height,
        align_top: quoteFields.customFields.align_top,
      },
    ],
  };
}
function formBuilder(formRaw) {
  return {
    title: formRaw.header,
    description: formRaw.blurb,
    form: {
      formId: formRaw.formId,
      form_header: '',
      datalayer: formRaw.formDataLayer,
      form_required_text: '',
    },
  };
}
function resourceCardsBuilder(sectionRaw) {
  const cards = sectionRaw.card.map((card) => {
    const cardFields = card.fields;
    return {
      icon: {
        name: cardFields.iconName,
        alt: `${cardFields.iconName} Icon`,
        variant: 'marketing',
      },
      event_type: cardFields.subtitle,
      header: cardFields.title,
      link_text: cardFields.button.fields.text,
      href: cardFields.cardLink,
      image: cardFields.image.fields.file.url,
      data_ga_name: cardFields.cardLinkDataGaName,
      data_ga_location: cardFields.cardLinkDataGaLocation,
    };
  });
  return {
    column_size: 4,
    cards,
  };
}
function reportCTABuilder(sectionRaw) {
  const reports = sectionRaw.card.map((report) => {
    const cardFields = report.fields;
    return {
      description: cardFields.title,
      link_text: cardFields.button.fields.text,
      url: cardFields.cardLink,
    };
  });
  return {
    layout: 'dark',
    title: sectionRaw.header,
    reports,
  };
}
function securityCardsBuilder(sectionRaw) {
  const cards = sectionRaw.card.map((card) => {
    const cardFields = card.fields;
    return {
      icon: {
        name: cardFields.iconName,
      },
      title: cardFields.title,

      link: {
        text: cardFields.button.fields.text,
        url: cardFields.cardLink,
        data_ga_name: cardFields.cardLinkDataGaName,
        data_ga_location: cardFields.cardLinkDataGaLocation,
      },
    };
  });
  return {
    layout: 'dark',
    cards,
  };
}
function pageCleanUp(page) {
  let nextSteps;
  const pageData = page.map((section) => {
    const id =
      section.fields?.componentName ||
      section.fields?.componentId ||
      section.sys?.contentType?.sys?.id;
    switch (id) {
      case 'solutions-hero':
        return {
          name: id,
          data: { ...solutionsHeroBuilder(section.fields) },
        };
      case 'security-resources-feature':
        return {
          name: id,
          data: { ...securityResourcesBuilder(section.fields) },
        };
      case 'quotes-carousel':
        return {
          name: id,
          data: { ...quoteCarouselBuilder(section.fields) },
        };
      case 'sts-marketo-form':
        return {
          name: id,
          data: { ...formBuilder(section.fields) },
        };
      case 'solutions-resource-cards':
        return {
          name: id,
          data: { ...resourceCardsBuilder(section.fields) },
        };
      case 'security-cta-section':
        return {
          name: id,
          data: { ...securityCardsBuilder(section.fields) },
        };
      case 'report-cta':
        return {
          name: id,
          data: { ...reportCTABuilder(section.fields) },
        };
      case 'nextSteps':
        nextSteps = section.fields.variant || '1';
        return { name: null, data: null };
      default:
        return { name: null, data: null };
    }
  });
  return { pageData, nextSteps };
}

export function valueStreamsHelper(data) {
  const { pageContent, seoMetadata } = data;

  const { pageData, nextSteps } = pageCleanUp(pageContent);

  const page: ValueStreamsData = {
    ...metaDataHelper(seoMetadata[0]),
    components: pageData,
    nextSteps,
  };

  return page;
}
