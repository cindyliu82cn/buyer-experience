import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { convertToCaseSensitiveLocale } from '~/common/util';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfEntry,
  CtfCardGroup,
  CtfFaq,
  CtfCard,
  CtfButton,
  CtfImage,
  CtfPricingCard,
} from '~/models';

interface CtfPricingEntry {
  faq?: any;
  next_steps?: any;
  addons?: any;
}
export class PricingService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private mapNextStepsCards(cardsObject: CtfCardGroup) {
    const cards = cardsObject.card.map((item: CtfEntry<CtfCard>) => {
      const link: CtfButton = item.fields?.button?.fields;
      const image: CtfImage = item.fields?.image?.fields;
      return {
        tagline: item.fields.title,
        avatar: image && image.file.url,
        link: link && {
          text: link.text,
          url: item.fields.cardLink,
          data_ga_name: item.fields.cardLinkDataGaName,
          data_ga_location: item.fields.cardLinkDataGaLocation,
        },
      };
    });
    return cards;
  }
  private mapAddonCard(item: CtfEntry<CtfPricingCard>) {
    const link: CtfButton = item.fields?.button?.fields;
    return {
      heading: item.fields?.header,
      price: item.fields?.price,
      price_info: item.fields?.label,
      description: item.fields?.priceDescription,
      hide_on_mobile: item.fields?.pricingFeatures ? true : false,
      features: item.fields?.pricingFeatures && {
        list: item.fields?.pricingFeatures?.map((item: CtfEntry<any>) => {
          return {
            text: item.fields?.text?.content[0]?.content[0]?.value,
          };
        }),
      },
      link: link && {
        text: link.text,
        url: link.externalUrl,
        data_ga_name: link.dataGaName,
        data_ga_location: link.dataGaLocation,
      },
    };
  }

  private mapFeaturedAddonCard(item: CtfEntry<CtfPricingCard>) {
    const link: CtfButton = item.fields?.button?.fields;
    const pricingCard = item.fields?.contentArea
      ? item.fields?.contentArea[0]
      : null;
    const pricingCardValues = pricingCard && this.mapAddonCard(pricingCard);
    return {
      ...pricingCardValues,
      title: item.fields?.title,
      featured: true,
      pill: item.fields?.pills[0],
      list_footnote: item.fields?.description,
      card_note:
        pricingCard &&
        pricingCard.fields.footnote?.content[0]?.content[0]?.value,
      self_managed_note:
        pricingCard &&
        pricingCard.fields.pricingFeatures[0]?.fields?.text?.content[0]
          ?.content[0]?.value,
      features: item.fields?.list && {
        list: item.fields?.list.map((item) => {
          return {
            text: item,
          };
        }),
      },
      link: link && {
        text: link.text,
        url: item.fields?.cardLink,
        data_ga_name: item.fields?.cardLinkDataGaName,
        data_ga_location: item.fields?.cardLinkDataGaLocation,
      },
    };
  }
  private mapAddonsCards(cardsObject: CtfCardGroup) {
    const cards = cardsObject?.card.map((item: CtfEntry<CtfPricingCard>) => {
      if (item.sys.contentType.sys.id == CONTENT_TYPES.CARD) {
        return this.mapFeaturedAddonCard(item);
      } else {
        return this.mapAddonCard(item);
      }
    });
    return {
      heading: cardsObject.header,
      cards,
    };
  }

  private faqItems(group: CtfEntry<CtfFaq>[]) {
    return group.map((item) => {
      return {
        // FAQ components are missing an internalName field, to keep our format of adding locale prefixes to a content entry name we'll use the subtitle fiels as header
        header: item.fields.subtitle,
        questions: item.fields.singleAccordionGroupItems.map(
          (accordion: CtfEntry<any>) => {
            return {
              question: accordion.fields.header,
              answer: accordion.fields.text,
            };
          },
        ),
      };
    });
  }
  private faqHelper(data: CtfFaq) {
    const questions =
      data.multipleAccordionGroups &&
      this.faqItems(data.multipleAccordionGroups);

    return {
      // FAQ components are missing an internalName field, to keep our format of adding locale prefixes to a content entry name we'll use the subtitle fiels as header
      header: data.subtitle,
      data_inbound_analytics: data.dataInboundAnalytics,
      background: data.background,
      show_all_button: data.showAllButton,
      jump_down_button_text: data.jumpDownButtonText,
      texts: {
        show: data.expandAllCarouselItemsText,
        hide: data.hideAllCarouselItemsText,
      },
      groups: questions,
    };
  }

  private async getContentfulPricingPage() {
    const pricingEntry = await getClient().getEntries({
      content_type: 'page',
      include: 10,
      'fields.slug': 'pricing',
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });
    if (pricingEntry.items.length === 0) {
      throw new Error('Pricing landing page not found in Contentful');
    }
    const [items] = pricingEntry.items;
    return this.transformContentfulData(items);
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;
    const mappedContent = this.getCtfComponents(pageContent);
    return { ...seoMetadata[0].fields, ...mappedContent };
  }

  private getCtfComponents(pageContent: CtfEntry<any>[]) {
    const result: CtfPricingEntry = {};
    for (const component of pageContent) {
      const componentName =
        component.fields.componentName ?? component.fields.id;
      switch (componentName) {
        case 'faq': {
          result.faq = this.faqHelper(component.fields);
          break;
        }
        case 'addons': {
          result[componentName] = this.mapAddonsCards(component.fields);
          break;
        }
        case 'next_steps': {
          result[componentName] = this.mapNextStepsCards(component.fields);
          break;
        }
      }
    }
    return result;
  }

  async getPricingPageContent() {
    const pricingPage = await this.getContentfulPricingPage();

    return pricingPage;
  }
}
