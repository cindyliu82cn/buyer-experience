import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful';
import {
  CtfEntry,
  CtfEventPage,
  CtfExternalVideo,
  CtfHeaderAndText,
  CtfHero,
  CtfPage,
  CtfVideoCarousel,
} from '~/models';
import {
  imageBuilder,
  metaDataHelper,
  speakerBuilder,
} from '~/services/events/events.helpers';
import { getUrlFromContentfulImage } from '~/common/util';

export class EventsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private async getContentfulChildEvent(slug: string) {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT,
      'fields.slug': slug,
      include: 4,
    });

    if (items.length === 0) {
      throw new Error('Event page not found in Contentful');
    }

    return items;
  }

  private async getContentfulLandingEvents() {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT_CARD,
      order: 'fields.date',
      limit: 36,
      'fields.endDate[gte]': new Date().toISOString().split('T')[0],
    });

    return items;
  }

  private async getContentfulLandingPage() {
    const { items } = await getClient().getEntries({
      content_type: 'page',
      include: 10,
      'fields.slug': '/events/',
    });

    if (items.length === 0) {
      throw new Error('Event landing page not found in Contentful');
    }

    const [landingPage] = items;

    return landingPage;
  }

  async getLandingEventsContent() {
    const landingEvents = await this.getContentfulLandingEvents();
    const eventPage = await this.getContentfulLandingPage();
    const eventsCards = landingEvents.map((event) => event.fields);
    const { entryId, spaceId, ...eventsPageData } =
      this.transformLandingEventData(eventPage);

    return {
      eventsCards,
      eventsPageData,
      entryId,
      spaceId,
    };
  }

  async getChildEventContent(slug: string) {
    const items = await this.getContentfulChildEvent(slug);
    const [eventPage] = items;

    return {
      items,
      content: this.transformEventChildData(eventPage.fields),
      spaceId: eventPage.sys.space.sys.id,
      entryId: eventPage.sys.id,
    };
  }

  transformLandingEventData({ fields, sys }: CtfEntry<CtfPage>) {
    const spaceId = sys.space.sys.id;
    const entryId = sys.id;

    const videoData: CtfVideoCarousel = fields.pageContent?.find(
      (obj) => obj.sys.contentType.sys.id === CONTENT_TYPES.VIDEO_CAROUSEL,
    )?.fields;

    const heroData: CtfHeaderAndText = fields.pageContent?.find(
      (obj) => obj.sys.contentType.sys.id === CONTENT_TYPES.HEADER_AND_TEXT,
    )?.fields;

    // Iterate through each video in the carousel
    const mappedVideos = videoData?.videos?.map(
      (video: CtfEntry<CtfExternalVideo>) => ({
        title: video.fields?.internalName,
        video_link: video.fields?.url,
        photourl: getUrlFromContentfulImage(
          video.fields?.thumbnail?.fields?.image,
        ),
        carousel_identifier: ['Event Videos'],
      }),
    );

    const hero = { ...heroData, title: heroData?.header };
    const seoMetadata = fields.seoMetadata[0]?.fields;

    return {
      hero,
      seoMetadata,
      videoCarousel: {
        header: 'Event Videos',
        videos: mappedVideos,
      },
      spaceId,
      entryId,
    };
  }

  transformEventChildData(data: CtfEventPage) {
    const rawData = this.dataCleanUp(data);

    return {
      metadata: metaDataHelper(rawData),
      name: rawData.name || '',
      internalName: rawData.internalName || '',
      slug: rawData.slug || '',
      eventType: rawData.eventType || '',
      description: rawData.description || '',
      date: rawData.date || '',
      endDate: rawData.endDate || '',
      location: rawData.location || '',
      region: rawData.region || '',
      hero:
        rawData.hero &&
        this.heroHelper(
          rawData.hero.fields,
          rawData.name,
          rawData.staticFields,
        ),
      booth: this.sectionsHelper(rawData.booth),
      featuredContent: rawData.featuredContent,
      social: this.sectionsHelper(rawData.social),
      agenda: this.agendaHelper(rawData.agenda),
      sessions: this.sectionsHelper(rawData.sessions, 'speakers'),
      careers: this.careersHelper(rawData.careers),
      footnote: rawData.footnote,
      nextSteps: rawData.nextSteps?.fields.variant || 1,
    };
  }

  private dataCleanUp(data: CtfEventPage) {
    const mappedData = {};

    Object.keys(data).forEach((key) => {
      if (Array.isArray(data[key])) {
        mappedData[key] = this.mapArray(data[key]);
      } else {
        mappedData[key] = data[key];
      }
    });

    return mappedData;
  }

  private mapArray(array) {
    return array
      .filter((element) => element.fields)
      .map((element) => {
        if (element.fields.assets) {
          element.fields.assets = element.fields.assets.map(
            (asset) => asset.fields,
          );
        }
        if (element.fields.cta) {
          element.fields.cta = element.fields.cta.fields;
        }
        return element.fields;
      });
  }

  private heroHelper(hero: CtfHero, eventName: string, staticFields: any) {
    return {
      headline: eventName || '',
      accent: hero?.title || '',
      subHeading: hero?.subheader || '',
      list: hero.description?.split('\n') || [],
      cta: hero?.primaryCta?.fields || {},
      icons: staticFields?.icons,
      darkMode: staticFields.darkMode,
    };
  }

  private sectionsHelper(data, key?) {
    let sections = [];
    if (data) {
      sections = data.map((section) => {
        const { header, text, subheader, cta, assets } = section;
        let parsedAssets = assets;
        if (assets) {
          parsedAssets = assets.map((asset) => {
            if (asset.image) {
              return imageBuilder(asset.image?.fields);
            }
            if (asset.authorHeadshot) {
              return speakerBuilder(asset);
            }
            return parsedAssets;
          });
        }
        return {
          header,
          text,
          subheader,
          cta,
          [key || 'assets']: parsedAssets,
        };
      });
    }
    return sections;
  }

  private agendaHelper(data) {
    let agenda;
    if (data) {
      agenda = { ...data?.shift(), schedule: [...data] };
    }
    return agenda;
  }

  private careersHelper(data) {
    let careersContent = null;
    if (data) {
      careersContent = { ...data[0] };
    }
    return careersContent;
  }
}
