import { Context } from '@nuxt/types';
import { convertToCaseSensitiveLocale } from '~/common/util';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class FreeTrialService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    CTA: 'call-to-action',
    FREE_TRIAL_PLANS: 'free-trial-plans',
    FAQ: 'faq',
    NEXT_STEPS: 'next_steps',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /free-trial page
   * @param slug
   */
  async getContent(slug: string, i18n: any) {
    try {
      return this.getContentfulData(slug, i18n);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string, i18n: any) {
    try {
      const props = {
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      };

      if (i18n) {
        props['locale'] = convertToCaseSensitiveLocale(i18n.locale || i18n);
      }
      const content = await getClient().getEntries(props);

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [freeTrial] = content.items;

      return this.transformContentfulData(freeTrial);
    } catch (e) {
      throw new Error(e);
    }
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      pageContent,
      components: mappedContent,
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      components.push(this.mapCtfComponent(ctfComponent));
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      // FREE TRIAL LANDING PAGE COMPONENTS

      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapCta(ctfComponent);
        break;
      }

      case CONTENT_TYPES.FAQ: {
        component = this.mapFaq(ctfComponent);
        break;
      }

      case CONTENT_TYPES.TAB_CONTROLS_CONTAINER: {
        component = this.mapFreeTrialPlans(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  mapFaq(ctfFaq: any) {
    const {
      title,
      multipleAccordionGroups,
      singleAccordionGroupItems,
      showAllButton,
      background,
    } = ctfFaq.fields;

    if (singleAccordionGroupItems) {
      const questions = singleAccordionGroupItems.map((element: any) => ({
        question: element.fields.header,
        answer: element.fields.text,
      }));

      return {
        header: title,
        show_all_button: showAllButton,
        background,
        groups: [{ header: '', questions }],
      };
    }

    const groups = multipleAccordionGroups.map((group: any) => {
      const questions = group.fields.singleAccordionGroupItems.map(
        (element: any) => ({
          question: element.fields.header,
          answer: element.fields.text,
        }),
      );

      return {
        header: group.fields.title,
        questions,
      };
    });

    return {
      name: this.componentNames.FAQ,
      data: {
        header: title,
        groups,
      },
    };
  }

  private mapCta(ctfCta: any) {
    const { fields } = ctfCta;

    return {
      name: this.componentNames.CTA,
      data: {
        title: fields.header,
        subtitle: fields.text,
        centered_by_default: true,
        aos_animation: 'fade-down',
        aos_duration: 500,
      },
    };
  }

  private mapFreeTrialPlans(ctfFreeTrialPlans: any) {
    const { fields } = ctfFreeTrialPlans;

    let gitlabSaas: Object = {};
    let selfManaged: Object = {};

    fields.tabs.forEach((tab) => {
      const id = tab.fields.tabId.trim();

      if (id === 'gitlab-self-managed') {
        selfManaged = {
          formId: tab.fields.tabPanelContent[0]?.fields?.formId,
          form_header: tab.fields.tabPanelContent[0]?.fields?.header,
        };
      } else if (id === 'gitlab-saas') {
        gitlabSaas = {
          text: tab.fields.tabPanelContent[0]?.fields?.title,
          tooltip_text:
            tab.fields.tabPanelContent[0]?.fields?.contentArea[0]?.fields?.text,
          bottom_text: tab.fields.tabPanelContent[0]?.fields?.description,
          link: {
            text: tab.fields.tabPanelContent[0]?.fields?.button?.fields?.text,
            href: tab.fields.tabPanelContent[0]?.fields?.button?.fields
              ?.externalUrl,
            data_ga_name:
              tab.fields.tabPanelContent[0]?.fields?.button?.fields?.dataGaName,
            data_ga_location:
              tab.fields.tabPanelContent[0]?.fields?.button?.fields
                ?.dataGaLocation,
          },
        };
      }
    });

    return {
      name: this.componentNames.FREE_TRIAL_PLANS,
      data: {
        saas: { ...gitlabSaas },
        self_managed: { ...selfManaged },
      },
    };
  }
}
