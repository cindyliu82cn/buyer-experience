import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfAnchorLink,
  CtfBlockGroup,
  CtfCardGroup,
  CtfEntry,
  CtfSideNavigation,
} from '~/models';
import { mapSolutionsHero } from '~/services/solutions/solutions-default.helper';

export class IntegrationsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getContent(slug: string) {
    const integrationsEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 10,
    });

    if (integrationsEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [items] = integrationsEntry.items;
    return { items, integrations: this.transformContentfulData(items) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getCtfComponents(pageContent);

    return {
      ...seoMetadata[0]?.fields,
      ...mappedContent,
    };
  }

  private getCtfComponents(pageContent: CtfEntry<any>[]) {
    const result = {};

    for (const component of pageContent) {
      const { id } = component.sys.contentType.sys;

      switch (id) {
        case CONTENT_TYPES.SIDE_MENU: {
          result.side_navigation_variant = this.mapSideNavigation(
            component.fields,
          );
          break;
        }
        case CONTENT_TYPES.HERO: {
          result.hero_block = mapSolutionsHero(component.fields).data;
          break;
        }
        case CONTENT_TYPES.BLOCK_GROUP: {
          result.integrations_blocks = this.mapBlockGroup(component.fields);
          break;
        }
      }
    }

    return result;
  }

  private mapBlockGroup(ctfBlockGroup: CtfBlockGroup) {
    return ctfBlockGroup.blocks.map(
      (block: CtfEntry<CtfCardGroup> | CtfEntry<CtfBlockGroup>) => {
        const { id } = block.sys.contentType.sys;
        switch (id) {
          case CONTENT_TYPES.CARD_GROUP: {
            return this.mapCardGroup(<CtfCardGroup>block.fields);
          }
          case CONTENT_TYPES.BLOCK_GROUP: {
            return this.mapInnerSections(<CtfBlockGroup>block.fields);
          }
        }
      },
    );
  }

  private mapCardGroup(ctfCardGroup: CtfCardGroup) {
    const items =
      ctfCardGroup.card?.map((card) => ({
        name: card.fields.title,
        description: card.fields.subtitle,
        text: card.fields.description,
        url: card.fields.cardLink,
      })) || [];

    return {
      title: ctfCardGroup.header,
      description: ctfCardGroup.subheader,
      subtitle: ctfCardGroup.description,
      call_to_action: ctfCardGroup.cta && {
        text: ctfCardGroup.cta.fields.text,
        url: ctfCardGroup.cta.fields.externalUrl,
      },
      items,
    };
  }

  private mapInnerSections(ctfBlockGroup: CtfBlockGroup) {
    return {
      title: ctfBlockGroup.header,
      sections: ctfBlockGroup.blocks.map((cardGroup: CtfEntry<CtfCardGroup>) =>
        this.mapCardGroup(cardGroup.fields),
      ),
    };
  }

  private mapSideNavigation(ctfSideMenu: CtfSideNavigation) {
    const links =
      ctfSideMenu.anchors?.map((anchor: CtfEntry<CtfAnchorLink>) => ({
        title: anchor.fields.linkText,
        href: anchor.fields.anchorLink,
      })) || [];

    return {
      text: ctfSideMenu.header,
      links,
    };
  }
}
