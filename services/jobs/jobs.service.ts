import { getUrlFromContentfulImage } from '~/common/util';

export function assemblePageContent(contentfulData) {
  const seoMetadata = contentfulData[0]?.fields.seoMetadata;
  const data = contentfulData[0]?.fields.pageContent;
  let components = [];
  let nextStepsVariant;

  // build metadata
  const seoImage = getUrlFromContentfulImage(
    seoMetadata?.[0]?.fields?.ogImage?.fields?.image,
  );

  const metadata = {
    ...(seoMetadata?.[0]?.fields ?? {}),
    twitter_image: seoImage,
    og_image: seoImage,
    image_title: seoImage,
  };

  // iterate over page content data to build components
  data.forEach((obj) => {
    switch (obj.sys.contentType.sys.id) {
      case 'eventHero':
        components.push({
          name: 'SolutionsHero',
          data: {
            title: obj.fields?.title,
            subtitle: obj.fields?.subheader,
            primary_btn: {
              text: obj.fields?.primaryCta?.fields?.text,
              url: obj.fields?.primaryCta?.fields?.externalUrl,
              icon: obj.fields?.primaryCta?.fields?.iconName
                ? {
                    name: obj.fields.primaryCta.fields.iconName,
                    variant:
                      obj.fields.primaryCta.fields.iconVariant || 'product',
                  }
                : null,
              data_ga_name: obj.fields?.primaryCta?.fields?.dataGaName,
              data_ga_location: obj.fields?.primaryCta?.fields?.dataGaLocation,
            },
            video: {
              video_url: obj.fields?.video?.fields?.url,
            },
            image: {
              image_url: obj.fields?.backgroundImage?.fields?.file?.url,
              alt: obj.fields?.backgroundImage?.fields?.title,
              bordered: obj.fields?.customFields?.bordered,
              image_purple_background:
                obj.fields?.customFields?.image_purple_background,
              is_video_thumbnail: obj.fields?.customFields?.is_video_thumbnail,
            },
          },
        });
        break;
      case 'headerAndText':
        if (obj.fields.componentName === 'copy-about') {
          components.push({
            name: 'copy-about',
            data: {
              title: obj.fields.header,
              description: obj.fields.text,
              cta_link: obj.fields.customFields?.cta_link?.cta_link,
              cta_text: obj.fields.customFields?.cta_link?.cta_text,
              data_ga_name: obj.fields.customFields?.cta_link?.data_ga_name,
              data_ga_location:
                obj.fields.customFields?.cta_link?.data_ga_location,
            },
          });
        }
        break;
      case 'card':
        if (obj.fields.componentName === 'badge') {
          components.push({
            name: 'badge-card',
            data: {
              title: obj.fields.title,
              text: obj.fields.subtitle || obj.fields.description,
              img: {
                src: getUrlFromContentfulImage(obj.fields.image),
                alt: obj.fields.image.fields.title || '',
              },
              cta: {
                href: obj.fields.button?.fields.externalUrl,
                text: obj.fields.button?.fields.text,
                dataGaName:
                  obj.fields.button?.fields.dataGaName ||
                  obj.fields.button?.fields.text.toLowerCase(),
                dataGaLocation:
                  obj.fields.button?.fields.dataGaLocation || 'body',
              },
            },
          });
        }
      case 'blockGroup':
        if (obj.fields.componentName === 'moments') {
          components.push({
            name: 'card-block',
            data: {
              header: obj.fields.header,
              subheader: obj.fields.subheader,
              button: { ...obj.fields.customFields },
              cards: obj.fields.blocks?.map((quote) => ({
                title: quote.fields.quoteText,
                subtitle: `${quote.fields.author}`,
                job: quote.fields.authorTitle && quote.fields.authorTitle,
                location:
                  quote.fields.authorCompany && quote.fields.authorCompany,
                image: {
                  src: getUrlFromContentfulImage(
                    quote.fields.authorHeadshot?.fields.image,
                  ),
                  alt: quote.fields.authorHeadshot?.fields.altText,
                },
              })),
            },
          });
        }
        break;
      case 'cardGroup':
        if (obj.fields.componentName === 'benefits') {
          components.push({
            name: 'benefits',
            data: {
              colsOverride: obj.fields.card?.length % 3 === 0 && 4,
              eyebrow: true,
              title: obj.fields.header,
              description: obj.fields.description,
              cards: obj.fields.card?.map((item) => ({
                title: item?.fields?.title,
                description: item?.fields?.description,
                icon: {
                  name: item?.fields?.iconName,
                  variant: 'marketing',
                  alt: item?.fields?.iconName,
                },
                href: item?.fields?.button?.fields?.externalUrl,
                cta: item?.fields?.button?.fields?.text,
                data_ga_name: item?.fields?.button?.fields?.dataGaName,
                data_ga_location: item?.fields?.button?.fields?.dataGaLocation,
              })),
            },
          });
        }
        break;
      case 'customComponent':
        if (obj.fields.name === 'copy-numbers') {
          components.push({
            name: 'copy-numbers',
            data: {
              noMargin: true,
              title: obj.fields.data?.data?.title,
              rows: obj.fields.data?.data?.rows,
              customer_logos_block: {
                showcased_enterprises: obj.fields.logo?.map((item) => ({
                  image_url: item?.fields?.image?.fields?.file?.url,
                  alt: item?.fields?.altText,
                  url: item?.fields?.link,
                  link_label: item?.fields?.altText,
                })),
              },
              cta_title: obj.fields.data?.data?.cta_title,
              cta_text: obj.fields.data?.data?.cta_text,
              cta_link: obj.fields.data?.data?.cta_link,
              data_ga_name: obj.fields.data?.data?.data_ga_name,
              data_ga_location: obj.fields.data?.data?.data_ga_location,
            },
          });
        }
        break;
      case 'nextSteps':
        nextStepsVariant = obj.fields.variant;
        break;
      default:
        break;
    }
  });

  const page = {
    metadata,
    components: components,
    ctaVariant: nextStepsVariant,
  };

  return page;
}
