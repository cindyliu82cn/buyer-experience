import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';
import { CtfCard, CtfEntry, CtfHeaderAndText, CtfHero } from '~/models';

export class CustomerSuccessManagementService {
  private readonly $ctx: Context;

  private readonly CONTENT_IDS = {
    hero: CONTENT_TYPES.HERO,
    headerAndText: CONTENT_TYPES.HEADER_AND_TEXT,
    card: CONTENT_TYPES.CARD,
    nextSteps: CONTENT_TYPES.NEXT_STEPS,
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /free-trial page
   * @param slug
   */
  async getContent(slug: string) {
    try {
      return this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    try {
      const props = {
        content_type: CONTENT_TYPES.PAGE,
        // 'sys.id': '5gi36PvbB8hYKSDAclK9Ww',
        'fields.slug': _slug,
        include: 4,
      };

      const content = await getClient().getEntries(props);

      if (content.items.length === 0) {
        throw new Error('Not found');
      }
      const [items] = content.items;
      return this.transformContentfulData(items);
    } catch (e) {
      throw new Error(e);
    }
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const { ctfHero, ctfHeaderAndText, ctfCards, ctfNextSteps } =
      this.getCtfComponents(pageContent);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      hero: this.mapHero(ctfHero),
      headerAndText: this.mapHeaderAndText(ctfHeaderAndText),
      nextSteps: ctfNextSteps,
      cards: ctfCards,
    };

    return transformedPage;
  }

  private getCtfComponents(ctfComponents: any) {
    const result: {
      ctfHero?: CtfHero;
      ctfHeaderAndText?: CtfHeaderAndText;
      ctfCards?: CtfCard[];
      ctfNextSteps?: any;
    } = {
      ctfCards: [],
    };

    for (const pageComponent of ctfComponents) {
      switch (pageComponent.sys.contentType.sys.id) {
        case this.CONTENT_IDS.hero:
          result.ctfHero = pageComponent;
          break;
        case this.CONTENT_IDS.headerAndText:
          result.ctfHeaderAndText = pageComponent;
          break;
        case this.CONTENT_IDS.card:
          result.ctfCards.push(pageComponent);
          break;
        case this.CONTENT_IDS.nextSteps:
          result.ctfNextSteps = pageComponent;
          break;
      }
    }

    return result;
  }

  private mapHero(ctfHero: CtfEntry<CtfHero>) {
    const mapping = {
      note: [ctfHero.fields.subheader],
      title: ctfHero.fields.title,
      image: {
        image_url: `${ctfHero.fields.backgroundImage.fields.file.url}?w=1000`,
        alt: ctfHero.fields.backgroundImage.fields.description,
      },
      bordered: true,
    };

    return mapping;
  }

  private mapHeaderAndText(ctfHeaderAndText: CtfEntry<CtfHeaderAndText>) {
    const mapping = {
      header: ctfHeaderAndText.fields.header,
      blocks: [{ text: ctfHeaderAndText.fields.text }],
      column_size: 8,
    };

    return mapping;
  }

  private mapNextSteps(ctfNextSteps: any) {
    return ctfNextSteps;
  }
}
