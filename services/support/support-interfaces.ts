interface SupportHero {
  title?: string | null;
  content?: string | null;
}

interface Anchors {
  text: string;
  data: [
    {
      text: string;
      href: string;
      nodes?: [
        {
          text: string;
          href: string;
          nodes?: [
            {
              text: string;
              href: string;
            },
          ];
        },
      ];
    },
  ];
}

interface Hyperlinks {
  text: string;
  data: any;
}

interface SideMenu {
  anchors: Anchors;
  hyperlinks?: Hyperlinks;
}

// accounts for headerAndText, TabControls, and Card content types
interface Section {
  header?: string | null;
  headerId?: string | null;
  text?: string;
  icon?: string;
  title?: string;
  id?: string;
  options?: [];
  select_menu?: {
    placeholder?: string;
    options?: [];
  };
  name?: string;
  cta?: {
    text?: string;
    href?: string;
    data_ga_name?: string;
    data_ga_location?: string;
  };
}

export interface SupportChildData {
  supportHero: SupportHero;
  side_menu: SideMenu;
  components: Section[];
}
