---
  title: Plattform
  description: Erfahre mehr darüber, wie die GitLab-Plattform Teams dabei helfen kann, zusammenzuarbeiten und Software schneller zu entwickeln.
  hero:
    note: Die umfangreichste
    header: KI-gestützte
    sub_header: DevSecOps-Plattform
    description: Liefere bessere Software schneller mit einer Plattform für deinen gesamten Softwareentwicklungszyklus.
    image:
      src: /nuxt-images/platform/loop-sheild-duo.svg
      alt: Der DevOps-Lebenszyklus - Planen, Codieren, Erstellen, Testen, Freigeben, Bereitstellen, Betreiben und Überwachen – ist in einem Unendlichkeitssymbol angeordnet, das sich mit dem Sicherheitssymbol überschneidet.
    button:
      text: Kostenlos testen
      href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Erfahre mehr über die Preise
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero

  features:
    title: Teams können mit einer einzigen Plattform mehr erreichen
    subtitle: Durch eine einzige integrierte Anwendung wird die Verwendung von DevSecOps über Teams und Software-Lebenszyklusphasen hinweg vereinfacht
    viewAllLink:
      view_all_text: Weitere Lösungen
      view_all_url: /solutions/
      data_ga_name: solutions
      data_ga_location: body
    cards:
      - name: Künstliche Intelligenz und maschinelles Lernen
        description: Gestalte bestehende Softwarebereitstellungs-Workflows intelligenter und verbessere so die Produktivität und Effizienz.
        icon: ai-summarize-mr-review
        href: /gitlab-duo/
        data_ga_name: gitlab duo
        data_ga_location: features cards
      - name: Sicherheit der Softwarebereitstellungskette
        description: Erstelle eine Software-Leistungsbeschreibung (SBOM), überprüfe die Lizenzkonformität und automatisiere die Sicherheit während des gesamten Entwicklungsprozesses, um Risiken und Verzögerungen zu verringern.
        icon: shield-check-large
        href: /solutions/dev-sec-ops/
        data_ga_name: devsecops
        data_ga_location: features cards
      - name: Wertschöpfungsketten-Management
        description: Biete allen Stakeholdern im Unternehmen wertvolle Einblicke in jede Phase der Ideenfindung und Entwicklung.
        icon: visibility
        href: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: features cards
      - name: Quellcodemanagement
        description: Versionskontrolle, damit dein Entwicklungsteam zusammenarbeiten und die Produktivität maximieren kann. So kannst du schneller bereitstellen und erhältst mehr Transparenz.
        icon: cog-code
        href: /stages-devops-lifecycle/source-code-management/
        data_ga_name: source code management
        data_ga_location: features cards
      - name: Kontinuierliche Integration und kontinuierliche Bereitstellung (CI/CD)
        description: Automatisierung zum Erstellen, Testen und Bereitstellen deines Codes in deiner Produktionsumgebung.
        icon: continuous-integration
        href: /features/continuous-integration/
        data_ga_name: continuous integration
        data_ga_location: features cards
      - name: GitOps
        description: Infrastrukturautomatisierung und Zusammenarbeit für Cloud-Native-, Multi-Cloud- und Legacy-Umgebungen.
        icon: digital-transformation
        href: /solutions/gitops/
        data_ga_name: digital transformation
        data_ga_location: features cards
      - name: Agiles Projekt- und Portfoliomanagement
        description: Plane, initiiere, priorisiere und verwalte Innovationsinitiativen – mit vollständigem Überblick über die Arbeit deiner Teams.
        icon: agile
        href: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: features cards
    doubleCard:
      name: Finde den richtigen Tarif für dein Team
      icon: pricing-plans
      cta_one_text: GitLab Ultimate
      cta_one_link: /pricing/ultimate/
      data_ga_name_one: ultimate
      data_ga_location_one: features cards
      cta_two_text: GitLab Premium
      cta_two_link: /pricing/premium/
      data_ga_name_two: premium
      data_ga_location_two: features cards

  table:
    footer_text: Erfahre mehr über unsere Funktionen
    footer_link: /features/
    footer_data_ga_name: features
    footer_data_ga_location: table

  study_card:
    blurb: Forrester-Studie
    header: GitLab ermöglicht 427% ROI
    stats:
      - value: 12-fache
        text: Steigerung der Anzahl an jährlichen Veröffentlichungen
      - value: <6
        text: Monate Amortisationszeit
      - value: 87%
        text: verbesserte Entwicklungs- und Liefereffizienz
    button:
      href: https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html
      text: Studie lesen
      data_ga_name: forrester study
      data_ga_location: body

  benefits:
    title: Einheitliche Plattform
    subtitle: zur Förderung von Dev-, Sec- und Ops-Teams
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    cta:
      icon: time-is-money
      title: Wie viel kostet dich deine aktuelle Toolchain?
      text: Finde es heraus
      link: /calculator/roi/
      data_ga_name: roi calculator
      data_ga_location: body
    items:
      - header: DevSecOps der Spitzenklasse
        text:
          Eine All-in-One-DevSecOps-Lösung mit plattformweit integrierter Sicherheit


          Ein einziges Datenmodell ermöglicht Einblicke in deinen gesamten DevSecOps-Lebenszyklus
      - header: KI-gestützte Workflows
        text:
          Steigere die Effizienz und verkürze die Zykluszeiten aller Benutzer(innen) mit Hilfe von KI in jeder Phase des Softwareentwicklungszyklus - von der Planung und Erstellung von Code bis hin zu Tests, Sicherheit und Überwachung
      - header: Flexible Bereitstellung
        text:
          SaaS für Kunden, die GitLab als Dienst nutzen möchten


          Selbstverwaltet für Kunden, die die Bereitstellung steuern möchten


          GitLab Dedicated, ein Single-Tenant-SaaS-Angebot für Kunden mit Anforderungen hinsichtlich Datenisolierung und Standort
      - header: Cloud-agnostisch
        text:
          Stelle überall bereit und ermögliche so eine Multi-Cloud-Strategie


          Vermeide Anbieterbindungen — keine Vorzugsbehandlung eines einzelnen Cloud-Anbieters
      - header: Integrierte Benutzererfahrung
        text:
          Reduziere den Kontextwechsel durch ein integriertes Erlebnis auf einer einzigen Plattform


          Schnelleres Onboarding und reduzierte laufende Schulungskosten mit einer Plattform, die den gesamten Lebenszyklus der Softwarebereitstellung abdeckt
      - header: Offener Kern
        text:
          Ermögliche schnelle Innovationen, da durch unser offenes Entwicklungsmodell jeder etwas beitragen kann


          Eine schnellere Iteration, während wir mit unseren Kunden und der Community zusammenarbeiten
      - header: Keine Überraschungskosten
        text:
          Feste Preise ohne zusätzliche Kosten für Extra-Funktionen

  quotes_carousel_block:
    controls:
      prev: previous case study
      next: next case study
    quotes:
      - main_video: https://www.youtube.com/embed/sT85nT9X2mM
        logo:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: 'Nasdaq'
        header: Wie Nasdaq die Cloud-Transformation mit GitLab Ultimate erreicht
        data_ga_name: Nasdaq
        data_ga_location: case studies
        url: https://www.youtube.com/embed/sT85nT9X2mM
        company: Nasdaq
      - quote: "GitLab bietet alles, was wir wollen, von Sicherheit über Leistung bis hin zu Tests und mehr."
        author: Chintan Parmar
        logo:
          url: /nuxt-images/logos/dunelm.svg
          alt: 'dunelm logo'
        role: Principal Platform Engineer
        statistic_samples:
          - data:
              highlight: 275%
              subtitle: mehr wöchentliche Bereitstellungen
        url: /customers/dunelm/
        header: Warum <a href='/customers/dunelm/' data-ga-name='dunelm' data-ga-location='case studies'>Dunelm</a> sich für GitLab entschieden hat, um die Sicherheit zu erhöhen
        data_ga_name: dunelm case study
        data_ga_location: case studies
        cta_text: Mehr lesen
        company: Dunelm
      - logo:
          url: /nuxt-images/logos/deutsche-telekom-logo-01.jpg
          alt: 'Deutsche Telekom-Logo'
        role: Business Owner IT, CI/CD-Hub
        quote: Die Markteinführungszeit war für uns ein wichtiges Thema. Bevor unsere Transformation zu Agile und DevOps begann, hatten wir in einigen Fällen Release-Zyklen von fast 18 Monaten. Wir konnten diese drastisch auf etwa 3 Monate reduzieren.
        author: Thorsten Bastian
        statistic_samples:
          - data:
              highlight: 6x
              subtitle: schnellere Markteinführung
          - data:
              highlight: 13 000
              subtitle: aktive GitLab-Benutzer(innen)
        url: /customers/deutsche-telekom/
        header: <a href='/customers/deutsche-telekom/' data-ga-name='deutsche telekom' data-ga-location='case studies'>Deutsche Telekom:</a> Vorantreiben der DevSecOps-Transformation mit GitLab
        data_ga_name: deutsche telekom
        data_ga_location: case studies
        cta_text: Mehr lesen
        company: Deutsche Telekom
      - logo:
          url: /nuxt-images/logos/carfax-logo.png
          alt: 'Carfax-Logo'
        quote: Mit DevSecOps steht die Sicherheit immer im Vordergrund. Sie ist Teil jedes einzelnen Prozessschritts und nicht leicht zu übersehen.
        author: Mark Portofe
        role: Director of Platform Engineering
        statistic_samples:
          - data:
              highlight: 20%
              subtitle: mehr Bereitstellungen im Jahresvergleich
          - data:
              highlight: 30%
              subtitle: der früher in SDLC gefundenen Sicherheitslücken
        url: /customers/carfax/
        header: <a href='/customers/carfax/' data-ga-name='carfax' data-ga-location='case studies'>CARFAX</a> verbessert die Sicherheit, verringert das Pipeline-Management und senkt die Kosten mit GitLab
        data_ga_name: carfax
        data_ga_location: case studies
        cta_text: Mehr lesen
        company: CARFAX
      - logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: 'iron mountain logo'
        role: Vice President of Enterprise Technology
        quote: GitLab hat uns die Grundlage und die Plattform zur Verfügung gestellt, um unser skaliertes agiles Framework zu ermöglichen. Wir sind nun in der Lage, innerhalb unserer Enterprise-IT-Teams und mit unseren wichtigsten Stakeholder zusammenzuarbeiten.
        author: Hayelom Tadesse
        statistic_samples:
          - data:
              highlight: 150 Tsd. USD
              subtitle: ungefähre Kosteneinsparung pro Jahr
          - data:
              highlight: 20 Std.
              subtitle: eingesparte Einarbeitungszeit pro Projekt
        url: /customers/iron-mountain/
        header: <a href='/customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='case studies'>Iron Mountain</a> treibt die DevOps-Entwicklung mit GitLab Ultimate voran
        data_ga_name: iron mountain
        data_ga_location: case studies
        cta_text: Mehr lesen
        company: Iron Mountain

  report_cta:
    layout: "light"
    statistics:
      - number: Mehr als 50%
        text: Fortune 100
      - number: Über 30 Mio.
        text: registrierte Benutzer(innen)
    title: GitLab ist die führende DevSecOps-Plattform
    reports:
    - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      image: /nuxt-images/logos/forrester-logo.svg
      data_ga_name: forrester
      data_ga_location: reports section
      link_text: Bericht lesen
    - description: GitLab als Marktführer im Gartner® Magic Quadrant 2023" für DevOps-Plattformen anerkannt
      url: /gartner-magic-quadrant/
      image: /nuxt-images/logos/gartner-logo.svg
      data_ga_name: gartner
      data_ga_location: reports section
      link_text: Bericht lesen
