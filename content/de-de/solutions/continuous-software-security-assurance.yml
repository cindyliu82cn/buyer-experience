---
  title: Kontinuierliche Software-Sicherheit
  description: Mit GitLab kannst Du Sicherheitsfunktionen ganz einfach in deinen DevSecOps-Lebenszyklus integrieren. Sicherheit und Compliance sind von Anfang an integriert und bieten dir die nötige Transparenz und Kontrolle, um die Integrität deiner Software zu schützen.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Kontinuierliche Software-Sicherheit
        subtitle: Mit integriertem DevSecOps die Sicherheit nach links verschieben
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlosen Testzeitraum starten
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab-Endlosschleife"
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: Übersicht
          href: '#overview'
        - title: Vorteile
          href: '#benefits'
        - title: Funktionen
          href: '#capabilities'
        - title: Preisgestaltung
          href: "#pricing"
        - title: Fallstudien
          href: '#case-studies'
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                text:
                  highlight: Mit GitLab kannst Du Sicherheitsfunktionen ganz einfach in deinen DevSecOps-Lebenszyklus integrieren.
                  description: Sicherheit und Compliance sind von Anfang an integriert und bieten dir die nötige Transparenz und Kontrolle, um die Integrität deiner Software zu schützen.
            - name: by-solution-benefits
              data:
                title: Sicherheit. Compliance. Integriert.
                image:
                  image_url: "/nuxt-images/resources/resources_11.jpeg"
                  alt: Arbeiten an laptops
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOps-Symbol
                      variant: marketing
                    header: Integrierte Tests und Abhilfemaßnahmen
                    text: Mit jedem Code-Commit stellt GitLab Entwickler(innen) umsetzbare Sicherheits- und Compliance-Ergebnisse zur Verfügung (https://docs.gitlab.com/ee/user/application_security/), um Abhilfemaßnahmen zu einem früheren Zeitpunkt im Lebenszyklus durchzuführen, während die Entwickler(innen) noch am Code arbeiten.
                  - icon:
                      name: continuous-integration
                      alt: Symbol für kontinuierliche Integration
                      variant: marketing
                    header: Verwaltung von Sicherheitslücken in der Software
                    text: GitLab unterstützt Sicherheitsexperten dabei, [verbleibende Sicherheitslücken](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center) zu schließen.
                  - icon:
                      name: cloud-tick
                      alt: Symbol für kontinuierliche Integration
                      variant: marketing
                    header: Sicherheit für Cloud-Native-Anwendungen
                    text: GitLab hilft dir dabei, deine Cloud Native-Anwendungen und die Infrastruktur, von der sie abhängen, zu schützen, einschließlich Containern, Infrastructure-as-Code und APIs.
                  - icon:
                      name: automated-code
                      alt: Symbol für automatisierten Code
                      variant: marketing
                    header: Leitlinien und automatisierte Richtlinien
                    text: Die richtlinienkonformen Pipelines, MR-Genehmigungen, End-to-End-Transparenz von Audit-Ereignissen sowie die integrierten [gemeinsamen Kontrollen](https://docs.gitlab.com/ee/administration/compliance.html) unterstützen dich dabei, deine Software-Lieferkette zu sichern und deine [Compliance-Anforderungen](https://about.gitlab.com/solutions/compliance/) zu erfüllen.
        - name: 'by-solution-value-prop'
          id: benefits
          data:
            title: Gib Entwickler(innen) freie Hand, damit sie schnell – und sicher – arbeiten können
            cards:
              - title: Vereinfachung
                description: Eine Plattform, ein Preis, mit umfassender Anwendungssicherheit.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/" data-ga-name="application security testing" data-ga-location="body">Anwendungssicherheitstests</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/vulnerability_report/" data-ga-name="vulnerability management" data-ga-location="body">Schwachstellenmanagement</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/container_scanning/" data-ga-name="deployed images" data-ga-location="body">Scan bereitgestellter Bilder</a>
                icon:
                  name: release
                  alt: Symbol für Agilität
                  variant: marketing
              - title: Sichtbarkeit
                description: Sieh, wer was, wo, wann und durchgängig geändert hat.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_events.html" data-ga-name="audit events" data-ga-location="body">Auditereignisse</a>
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_reports.html" data-ga-name="audit reports" data-ga-location="body">Auditberichte</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/dependency_list/#dependency-list" data-ga-name="dependency list" data-ga-location="body">Abhängigkeitsliste (Stückliste)</a>
                icon:
                  name: magnifying-glass-code
                  alt: Lupencode-Symbol
                  variant: marketing
              - title: Kontrolle
                description: Compliance-Framework für Konsistenz, gemeinsame Kontrollen, Richtlinienautomatisierung.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/compliance.html" data-ga-name="compliance capabilites" data-ga-location="body">Gemeinsame Compliance-Kontrollen</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/configuration/" data-ga-name="security policy configuration" data-ga-location="body">Konfiguration derSicherheitsrichtlinie</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration" data-ga-name="compliant pipelines" data-ga-location="body">Konforme Rohrleitungen</a>
                icon:
                  name: less-risk
                  alt: Symbol für weniger Risiko
                  variant: marketing
        - name: 'home-solutions-container'
          id: capabilities
          data:
            title: Sicherheit der DevSecOps-Plattform
            image: /nuxt-images/home/solutions/solutions-top-down.png
            alt: "Ein Büro aus Vogelperspektive"
            description: Besuche unser Trust Center, um zu sehen, wie wir die [GitLab-Software](https://about.gitlab.com/security/) sichern und die Branchenstandards einhalten.
            white_bg: true
            animation_type: fade
            solutions:
              - title: Tests innerhalb der CI-Pipeline
                description: Nutze deine Scanner oder unsere. Verschiebe die Sicherheit nach links, damit Entwickler(innen) Sicherheitslücken finden und beheben können, sobald sie entstehen. Umfangreiche Scanner beinhalten SAST, DAST, Secrets, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests.
                link_text: Mehr erfahren
                link_url: https://docs.gitlab.com/ee/user/application_security/
                data_ga_name: Test CI pipeline
                data_ga_location: body
                icon:
                  name: pipeline-alt
                  alt: Pipeline-Symbol
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_1.png
                alt: "Textblasen von kommunizierenden Teams"
              - title: Abhängigkeiten prüfen
                description: Abhängigkeiten und Container auf Sicherheitslücken scannen. Verwendete Abhängigkeiten inventarisieren.
                icon:
                  name: visibility
                  alt: Sichtbarkeitssymbol
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_2.png
                alt: "Textblasen von kommunizierenden Teams"
              - title: Sichere Cloud-native Apps
                description: Teste die Sicherheit von Cloud-nativen Elementen wie Infrastructure-as-a-Code, APIs und Cluster-Images.
                icon:
                  name: cloud-tick
                  alt: Cloud-Tick-Symbol
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_3.png
                alt: "Textblasen von kommunizierenden Teams"
              - title: Verwalten von Sicherheitslücken
                description: Entwickelt für Sicherheitsprofis, um Sicherheitslücken in Pipelines, On-Demand-Scans, bei Drittanbietern sowie aus Bug Bounties an einem Ort zu prüfen, zu bewerten und zu verwalten. Sofortige Sichtbarkeit, wenn Sicherheitslücken zusammengeführt werden. Einfachere Zusammenarbeit bei der Behebung von Schwachstellen
                icon:
                  name: continuous-integration
                  alt: Symbol für kontinuierliche Integration
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_4.png
                alt: "Textblasen von kommunizierenden Teams"
              - title: Schütze deine Software-Lieferkette
                description: Automatisiere Sicherheits- und Compliance-Richtlinien über den gesamten Lebenszyklus deiner Softwareentwicklung. Konforme Pipelines stellen sicher, dass die Pipeline-Richtlinien nicht umgangen werden, während gemeinsame Kontrollen für durchgängige Leitplanken sorgen.
                icon:
                  name: shield-check
                  alt: Schildprüfsymbol
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_5.png
                alt: "Textblasen von kommunizierenden Teams"
        - name: 'tier-block'
          class: 'slp-mt-96'
          id: 'pricing'
          data:
            header: Welche Stufe ist die richtige für dich?
            tiers:
              - id: kostenlos
                title: Kostenlos
                items:
                  - Statische Anwendungssicherheitstests (SAST) und Erkennung von Geheimnissen
                  - Funde in json-Datei
                link:
                  href: /pricing
                  text: Los geht's
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: kostenlose stufe
              - id: premium
                title: Premium
                items:
                  - Statische Anwendungssicherheitstests (SAST) und Erkennung von Geheimnissen
                  - Funde in json-Datei
                  - MR-Genehmigungen und weitere gängige Kontrollen
                link:
                  href: /pricing
                  text: Mehr erfahren
                  data_ga_name: pricing
                  data_ga_location: premium tier
                  aria_label: premiumstufe
              - id: ultimate
                title: Ultimate
                items:
                  - Alles in Premium plus
                  - Umfassende Sicherheitsscanner wie SAST, DAST, Secrets, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests
                  - Verwertbare Ergebnisse innerhalb der MR-Pipeline
                  - Compliance-Pipelines
                  - Dashboards für Sicherheit und Compliance
                  - Viel mehr
                link:
                  href: /pricing
                  text: Mehr erfahren
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: ultimate-stufe
                cta:
                  href: /free-trial/
                  text: Ultimate kostenlos testen
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: ultimate-stufe
        - name: 'by-industry-case-studies'
          id: case-studies
          data:
            title: Kundennutzen
            link:
              text: Alle Fallstudien
            charcoal_bg: true
            rows:
              - title: HackerOne
                subtitle: HackerOne erreicht 5x schnellere Implementierungen dank der integrierten Sicherheit von GitLab
                image:
                  url: /nuxt-images/software-faster/hackerone-showcase.png
                  alt: Computer mit Code
                button:
                  href: /customers/hackerone/
                  text: Mehr erfahren
                  data_ga_name: learn more
                  data_ga_location: body
              - title: The Zebra
                subtitle: Wie The Zebra sichere Pipelines in Schwarz-Weiß erzielt hat
                image:
                  url: nuxt-images/blogimages/thezebra_cover.jpg
                  alt: Handy macht Foto von einem Auto
                button:
                  href: /customers/thezebra/
                  text: Mehr erfahren
                  data_ga_name: learn more
                  data_ga_location: body
              - title: Hilti
                subtitle: Wie CI/CD und robustes Sicherheitsscanning den SDLC von Hilti beschleunigt haben
                image:
                  url: nuxt-images/blogimages/hilti_cover_image.jpg
                  alt: Wolkenkratzer bauen
                button:
                  href: /customers/hilti/
                  text: Mehr erfahren
                  data_ga_name: learn more
                  data_ga_location: body

    - name: group-buttons
      data:
        header:
          text: Entdecke weitere Möglichkeiten, wie GitLab zu kontinuierlicher Software-Sicherheit beitragen kann.
          link:
            text: Mehr Lösungen entdecken
            href: /solutions/
        buttons:
          - text: Automatisierung der Auslieferung
            icon_left: automated-code
            href: /solutions/delivery-automation/
          - text: Kontinuierliche Integration
            icon_left: continuous-delivery
            href: /solutions/continuous-integration/
          - text: Compliance.
            icon_left: shield-check
            href: /solutions/compliance/
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        title: Ressourcen
        link:
          text: Alle Ressourcen anzeigen
        cards:
          - icon:
              name: webcast
              alt: Webcast-Symbol
              variant: marketing
            event_type: Video
            header: DevSecOps-Übersichtsdemo
            link_text: Jetzt ansehen
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
            href: https://www.youtube.com/watch?v=2mmw3SF7Few
            aos_animation: fade-up
            aos_duration: 400
          - icon:
              name: webcast
              alt: Webcast-Symbol
              variant: marketing
            event_type: Video
            header: Erfahre, wie du Sicherheit in deine CICD-Pipeline einbaust
            link_text: Jetzt ansehen
            image: /nuxt-images/features/resources/resources_webcast.png
            href: https://youtu.be/Fd5DhebtScg?feature=shared
            aos_animation: fade-up
            aos_duration: 600
          - icon:
              name: webcast
              alt: Webcast-Symbol
              variant: marketing
            event_type: Video
            header: Effizientes Management von Sicherheitslücken und Risiken mit den GitLab Sicherheits-Dashboards
            link_text: Jetzt ansehen
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
            href: https://youtu.be/p3qt2z1rQk8
            aos_animation: fade-up
            aos_duration: 800
          - icon:
              name: webcast
              alt: Webcast-Symbol
              variant: marketing
            event_type: Video
            header: Verwalte deine Anwendungsabhängigkeiten
            link_text: Jetzt ansehen
            image: /nuxt-images/features/resources/resources_waves.png
            href: https://youtu.be/scNS4UuPvLI
            aos_animation: fade-up
            aos_duration: 1000
