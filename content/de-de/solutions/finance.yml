---
  title: GitLab für Finanzdienstleistungen
  description: Social Coding, kontinuierliche Integration und Release-Automatisierung haben sich bewährt, um die Entwicklung und Softwarequalität zu beschleunigen und gesetzte Ziele zu erreichen. Mehr erfahren!
  twitter_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  og_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  image_title: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        german_title: true
        title: GitLab für Finanzdienstleistungen
        subtitle: Die DevSecOps-Plattform für innovative Finanzinstitute
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimate kostenlos testen
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Erfahre mehr über die Preise
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          image_url_mobile: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          alt: "Bild: GitLab für Finanzdienstleistungen"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Goldman Sachs
            image: //images.ctfassets.net/xz1dnu24egyd/12sevPjM5gvLhceJRywIKq/36887bd972b75c0c43652532d7054aea/goldman-sachs.svg
            url: /customers/goldman-sachs/
            aria_label: Link zur Kunden-Fallstudie von Goldman Sachs
          - name: Bendigo and Adelaide Bank
            image: //images.ctfassets.net/xz1dnu24egyd/35U30Y5j0MbVQzS47dHgpL/611899a2437747143837287cf4f5f0e1/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Link zur Kunden-Fallstudie der Bendigo and Adelaide Bank 
          - name: Credit Agricole
            image: //images.ctfassets.net/xz1dnu24egyd/3oVtS30a8FdkABz7RDltNO/b4166b3f849fff7a0983b8d0a41619ad/credit-agricole-logo-2.png
            url: /customers/credit-agricole/
            aria_label: Link zur Kunden-Fallstudie von Credit Agricole
          - name: Worldline
            image: //images.ctfassets.net/xz1dnu24egyd/4OrazLr3PF3VyFLYLR5jvj/a63c02ce5f55b79ced4a7b0f718d68ca/worldline-logo-mono.png
            url: /customers/worldline/
            aria_label: Link zur Kunden-Fallstudie von Worldline
          - name: Moneyfarm
            image: //images.ctfassets.net/xz1dnu24egyd/42xuSR27SVba3Jxa6DpY65/f7dd7a330a782328dd19e4c8c194ff68/moneyfarm_logo.png
            url: /customers/moneyfarm/
            aria_label: Link zur Kunden-Fallstudie von Moneyfarm
          - name: UBS
            image: //images.ctfassets.net/xz1dnu24egyd/2n7QcbQlYLwERy3ItMgsrP/f5ab9507d15c11d97e3283d757702da8/ubs.svg
            url: /blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Link zur Kunden-Fallstudie von UBS
    - name: 'side-navigation-variant'
      links:
        - title: Übersicht
          href: '#overview'
        - title: Erfahrungsberichte
          href: '#testimonials'
        - title: Leistungen
          href: '#capabilities'
        - title: Vorteile
          href: '#benefits'
        - title: Fallstudien
          href: '#case-studies'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Sicherheit. Geschwindigkeit. Effizienz. 
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: "Symbol: DevSecOps"
                      variant: marketing
                    header: Verringere Sicherheits- und Konformitätsrisiken
                    text: Erkenne Schwachstellen und Sicherheitsprobleme früher im Entwicklungsprozess und setze Richtlinien durch, um die Konformitätsanforderungen einzuhalten.
                    link_text: Erfahre mehr über DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: increase
                      alt: "Symbol: Vergrößern"
                      variant: marketing
                    header: Schnellere Markteinführung
                    text: Verbessere die Erfahrung deiner Kunden und stelle ihnen neue Funktionen schneller zur Verfügung, indem du deinen Softwarebereitstellungsprozess mit einer vollständigen DevSecOps-Plattform automatisierst.
                    link_text: Mehr erfahren
                    link_url: /platform/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: piggy-bank-alt
                      alt: "Symbol: Sparschwein"
                      variant: marketing
                    header: Geringere Betriebskosten
                    text: Reduziere die Komplexität der Toolchain und ermögliche mehr Zusammenarbeit, Produktivität und Innovation mit einer einzigen Lösung.
                    link_text: Berechne deine Ersparnisse
                    link_url: /calculator/roi/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  Mit dem Vertrauen der Finanzdienstleistungsbranche.
                  <br />
                  Von Entwickler(innen) geliebt.
                quotes:
                  - title_img:
                      url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
                      alt: goldman sachs
                    quote: „GitLab hat es uns ermöglicht, die Geschwindigkeit der Entwicklung in unserer Entwicklungsabteilung drastisch zu erhöhen. Einige Teams führen jetzt täglich mehr als 1000 CI-Feature-Branch-Builds aus und führen sie zusammen!“
                    author: Andrew Knight
                    position: Managing Director, Goldman Sachs
                    ga_carousel: financial services goldman sachs
                  - title_img:
                      url: /nuxt-images/home/logo_ubs_mono.svg
                      alt: UBS Logo
                    quote: |
                      „Ohne GitLab hätten wir diese nahtlose Erfahrung nicht machen können. So konnten wir vielen unserer Konkurrenten zuvorkommen und die Barrieren zwischen der Programmierung, dem Testen und der Bereitstellung abbauen.“
                    author: Rick Carey
                    position: Group Chief Technology Officer, UBS
                    ga_carousel: financial services UBS
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: new10 Logo
                    quote: „GitLab hilft uns wirklich enorm bei unserer sehr modernen Architektur, denn es unterstützt Kubernetes, es unterstützt serverless und es unterstützt coole Sicherheitsfunktionen wie DAST und SAST. Dank GitLab haben wir eine wirklich hochmoderne Architektur.“
                    author: Kirill Kolyaskin
                    position: CTO, New10
                    ga_carousel: financial services new10
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Eine komplette DevSecOps-Plattform für Finanzdienstleistungen
                sub_description: 'Hilf deinen Kunden, ihre finanziellen Bedürfnisse effizienter und sicherer zu erfüllen, mit:'
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                alt: "Bild: Musterbeispiel"
                solutions:
                  - title: SBOM und Abhängigkeiten
                    description: Verschaffe dir einen klaren Überblick über die Abhängigkeiten oder die Software-Stückliste deines Projekts, einschließlich aller bekannten Schwachstellen und anderer wichtiger Details, wie z. B. den Paketierer, der die einzelnen Abhängigkeiten installiert hat, und die Softwarelizenz.
                    icon:
                      name: less-risk
                      alt: "Symbol: Geringeres Risiko"
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Umfassendes Sicherheits-Scanning
                    description: Erkenne Sicherheitslücken und sichere deine Anwendungen mit integrierten Sicherheitswerkzeugen wie SAST, Erkennung von Geheimnissen, Container-Scanning, Cluster-Image-Scanning, DAST, IaC-Scanning, API-Fuzzing und mehr.
                    icon:
                      name: monitor-pipeline
                      alt: Überwachung der Pipeline
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: Comprehensive security scanning
                    data_ga_location: solutions block
                  - title: Fuzz-Tests
                    description: Entdecke Programmfehler und potenzielle Sicherheitsprobleme, die bei anderen QA-Prozessen möglicherweise übersehen werden, indem du deine Pipelines in GitLab mit abdeckungsgesteuerten Fuzz-Tests erweiterst.
                    icon:
                      name: monitor-test
                      alt: "Symbol: Test überwachen"
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Gemeinsame Kontrollen für die Konformität
                    description: Automatisiere und erzwinge gemeinsame Kontrollen mit verschiedenen GitLab-Funktionen, von geschützten Zweigen und Umgebungen bis hin zu Genehmigungen für Merge Requests, Audit-Protokollen, Aufbewahrung von Artefakten und umfassenden Sicherheitsscans.
                    icon:
                      name: shield-check
                      alt: "Symbol: Schutzschild mit Häkchen"
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://about.gitlab.com/solutions/financial-services-regulatory-compliance/
                    data_ga_name: controls for compliance
                    data_ga_location: solutions block
                  - title: Konforme Workflow-Automatisierung
                    description: Stelle sicher, dass die Einstellungen, die dein Konformitätsteam konfiguriert, auch so konfiguriert bleiben und ordnungsgemäß mit Konformitäts-Frameworks und Pipelines funktionieren.
                    icon:
                      name: devsecops
                      alt: "Symbol: DevSecOps"
                      variant: marketing
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Einzigartig für Finanzdienstleistungen geeignet
                cards:
                  - title: Integriertes Sicherheits-Scanning
                    description: Gib Entwicklern die Möglichkeit, Sicherheitsprobleme zu finden und zu beheben, sobald sie entstehen. Nutze die in GitLab <a href="https://docs.gitlab.com/ee/user/application_security/">integrierten Scanner</a> – darunter SAST, DAST, Container-Scanning, IaC-Scanning und die Erkennung von Geheimnissen – und integriere eigene Scanner.
                    icon:
                      name: monitor-test
                      alt: "Symbol: Test überwachen"
                      variant: marketing
                  - title: Unterstützt deinen Weg in die Cloud
                    description: GitLab ist für Cloud-native Anwendungen konzipiert und verfügt über enge Integrationen mit <a href="https://about.gitlab.com/solutions/kubernetes/">Kubernetes</a>, <a href="https://about.gitlab.com/partners/technology-partners/aws/">AWS</a>, <a href="https://about.gitlab.com/partners/technology-partners/google-cloud-platform/" >Google Cloud</a> und <a href="https://about.gitlab.com/partners/technology-partners/#cloud-partners">anderen Cloud-Anbietern</a>.
                    icon:
                      name: gitlab-cloud
                      alt: "Symbol: GitLab-Cloud"
                      variant: marketing
                  - title: Lokal, selbst gehostet oder SaaS
                    description: GitLab funktioniert in allen Umgebungen. Du hast die Wahl.
                    icon:
                      name: monitor-gitlab
                      alt: "Symbol: Gitlab überwachen"
                      variant: marketing

        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Erfolgsgeschichten aus der Finanzbranche
                link:
                  text: Alle Fallstudien
                rows:
                  - title: Goldman Sachs
                    subtitle: Wie Dutzende von Teams bei Goldman Sachs in weniger als 24 Stunden in den produktiven Branch pushen
                    image:
                      url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
                      alt: Blick auf Gebäude von der Straße aus
                    button:
                      href: https://about.gitlab.com/customers/goldman-sachs/
                      text: Mehr erfahren
                      data_ga_name: goldman sachs learn more
                      data_ga_location: case studies
                  - title: Moneyfarm
                    subtitle: Dieses europäische Vermögensverwaltungsunternehmen verbesserte die Erfahrung seiner Entwickler und verdoppelte die Bereitstellungen
                    image:
                      url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
                      alt: Bild von Reflexionen in Gebäudefenstern
                    button:
                      href: https://about.gitlab.com/customers/moneyfarm/
                      text: Mehr erfahren
                      data_ga_name: uw learn more
                      data_ga_location: case studies
                  - title: Bendigo and Adelaide Bank
                    subtitle: Wie eine der vertrauenswürdigsten Banken Australiens die Cloud-Technologie einführte und die betriebliche Effizienz verbesserte
                    image:
                      url: /nuxt-images/blogimages/bab_cover_image.jpg
                      alt: Schwarz-Weiße Gebäude
                    button:
                      href: https://about.gitlab.com/customers/bab/
                      text: Mehr erfahren
                      data_ga_name: bendigo and adelaide learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLab-Veranstaltungen'
        description: "Nimm an einer Veranstaltung teil und erfahre, wie dein Team Software schneller und effizienter bereitstellen und gleichzeitig die Sicherheit und Konformität verbessern kann. Wir werden 2022 an zahlreichen Veranstaltungen teilnehmen, sie ausrichten und durchführen und können es kaum erwarten, dich dort zu sehen!"
        link: /events/
        button_text: Mehr erfahren
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: Menge während einer Präsentation
        icon: calendar-alt-2
