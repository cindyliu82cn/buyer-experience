title: GitLab Duo
description: The suite of AI capabilities powering your workflows
video:
  text: Meet GitLab Duo
  source: https://player.vimeo.com/video/855805049?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
hero: 
  note: GitLab Duo
  description: The suite of AI capabilities powering your workflows.
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: AI logo
  button:
    text: Try Free Ultimate Trial
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
journey:
  features:
    - name: AI-powered workflows
      description: Boost efficiency and reduce cycle times with the help of AI in every phase of the software development lifecycle.
    - name: Privacy-first, enterprise-grade
      description: We lead with a privacy-first approach to help enterprises and regulated organizations adopt AI-powered workflows.
    - name: Single application
      description: A single application with built-in security to deliver more software faster, enabling executive visibility across value streams and preventing context switching.
    - name: Empower every user at every step
      description: From planning and code creation to testing, security, and monitoring, our AI-assisted workflows support developer, security, and ops teams. 
featureCards:
  title: GitLab Duo Capabilities
  cards: 
    - name: Code Suggestions
      description: Enables you to write code more efficiently by viewing code suggestions as you type.
      icon: ai-code-suggestions
      href: https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html
    - name: Suggested Reviewers
      description: Helps you receive faster and higher-quality reviews by automatically finding the right people to review a merge request.
      icon: ai-reviewer-suggestions
      href: https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#suggested-reviewers
    - name: Value stream forecasting
      description: Predicts productivity metrics and identifies anomalies across your software development lifecycle.
      icon: ai-value-stream-forecast
      href: https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html
    - name: Discussion summary
      description: Quickly gets everyone up to speed on lengthy conversations to ensure you are all on the same page.
      icon: ai-issue
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-issue-discussions
    - name: Merge request summary
      description: Helps merge request authors drive alignment and action by efficiently communicating the impact of their changes.
      icon: ai-merge-request
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-my-merge-request-review
    - name: Vulnerability summary
      description: Helps developers remediate vulnerabilities more efficiently and uplevel their skills, enabling them to write more secure code.
      icon: ai-vulnerability-bug
      href: https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability
    - name: Code review summary
      description: Enables better handoffs between authors and reviewers and helps reviewers efficiently understand merge request suggestions.
      icon: ai-summarize-mr-review
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-merge-request-changes
    - name: Test generation
      description: Automates repetitive tasks and helps you catch bugs early.
      icon: ai-tests-in-mr
      href: https://docs.gitlab.com/ee/user/ai_features.html#generate-suggested-tests-in-merge-requests
    - name: Chat
      description: Helps you quickly identify useful information in large volumes of text, such as documentation.
      icon: ai-gitlab-chat
      href: https://docs.gitlab.com/ee/user/ai_features.html#gitlab-duo-chat
    - name: Code explanation
      description: Allows you to get up to speed quickly by explaining source code.
      icon: ai-code-explaination
      href: https://docs.gitlab.com/ee/user/ai_features.html#explain-selected-code-in-the-web-ui
principles:
  title: We help you build more secure software, faster
  slides: 
    - name: Data protection by design 
      description: Efficiency at the cost of privacy, security, and compliance is a nonstarter for you and us. With Code Suggestions, you keep your proprietary source code secure within GitLab's cloud infrastructure and this code isn't used as training data.
      image:
        src: /nuxt-images/solutions/ai/principle-1.png
    - name: Transparency at the core
      description: We provide the important information you need regarding our AI-assisted features and the ways in which we use and protect your data.
      image:
        src: /nuxt-images/solutions/ai/principle-2.png
    - name: Everyone can contribute
      description: We have an open core business model that enables us to build with our customers, who can contribute new capabilities to our product.
      image:
        src:  /nuxt-images/solutions/ai/principle-3.png
resources:
    data:
      title: What's new in GitLab AI
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: AI/ML in DevSecOps Series
          link_text: Read more
          image: /nuxt-images/blogimages/ai-ml-in-devsecops-blog-series.png
          href: /blog/2023/04/24/ai-ml-in-devsecops-series/ 
          data_ga_name: AI/ML in DevSecOps Series
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: >-
            GitLab details AI-assisted features in the DevSecOps platform
          link_text: Read more
          href: https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/ 
          image: /nuxt-images/blogimages/ai-fireside-chat.png
          data_ga_name: GitLab details AI-assisted features in the DevSecOps platform
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: How AI-assisted code suggestions will advance DevSecOps
          link_text: Read more
          href: https://about.gitlab.com/blog/2023/03/23/ai-assisted-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: How AI-assisted code suggestions will advance DevSecOps
          data_ga_location: body
report_cta:
  layout: "dark"
  title: Analyst reports
  reports:
  - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
    url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
  - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
    url: /gartner-magic-quadrant/
