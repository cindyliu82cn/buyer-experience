---
  data:
    competitor: JFrog
    gitlab_coverage: 50
    competitor_coverage: 100
    subheading: JFrog is a Universal Package Manager solution
    comparison_table:
      - stage: Package
        features:
          - feature: Package Registry
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: Every team needs a place to store their packages and dependencies. GitLab aims to provide a comprehensive solution, integrated into our single application, that supports package management for all commonly used languages and binary formats.
              details: |
                * GitLab offers Group and Project-level  private registries for a variety of package formats (11). List of supported package managers [here](https://docs.gitlab.com/ee/user/packages/package_registry/#supported-package-managers){data-ga-name="link to list of supported package managers" data-ga-location="body"}
                * Using GitLab Package Registry enables different custom workflows. Such as one project as Package Registry or multiple different packages from one monorepo
                * Using GitLab Package Registry brings with it the possibility to use GitLab CI/CD to build and publish packages and with that the flexibility to customize and adapt different workflows
              improving_product_capabilities: |
                * Richer [metadata](https://gitlab.com/groups/gitlab-org/-/epics/6207){data-ga-name="link to richer metadata" data-ga-location="body"} about the packages published in the Package Registry UI. With information about its security and compliance scans
                * Provide Package registries aggregations, to group diverse registries under a single URL
                * Provide support for more packages
                * Make packages more discoverable via search in the UI
                * In response to JFrog Edge Nodes there is an open [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/215390){data-ga-name="link to jfrog edge nodes open issue" data-ga-location="body"} addressing how to improve product capabilities
              link_to_documentation: https://docs.gitlab.com/ee/user/packages/package_registry/
            competitor:
              coverage: 100
              description: JFrog Artifactory focuses on a binary-centric approach -  it provides package registry and  management for an extensive list of package types. Providing  metadata and security scans information accessible through its UI once these binaries are uploaded to its repository
              details: |
                * JFrog is intentional about positioning themselves one step further after source code management. Claiming DevOps success is due to binary management and source code is a foundational step that plays a smaller role. Their focus becomes evident in the product UI and capabilities. Supporting as per documentation over 30 packages types and offering rich metadata about these ones. Metadata such as: security & compliance (Using JFrog Xray), type of license, number of downloads, version control, and a layered view of what is inside each binary
                * JFrog Artifactory allows for distributed replication of packages or images included in a release using Edge nodes
                * Offers [Artifactory Query language](https://www.jfrog.com/confluence/display/JFROG/Artifactory+Query+Language){data-ga-name="link to artifactory query language" data-ga-location="body"} to search artifacts and builds using different metadata fields, such as "license type"
                * Lacks collaboration tools within the platform, any conversation around a package vulnerability would need to be taken to another tool
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/Package+Management
          - feature: Container Registry
            gitlab:
              coverage: 50
              projected_coverage: 75
              description: A secure and private registry for Docker images built-in to GitLab. Creating, pushing, and retrieving images works out of the box with GitLab CI/CD.
              details: |
                * GitLab Container Registry is not a standalone registry. Being tightly integrated with GitLab - every project can have  its own space to store its Docker Images
                * Container Registry variables can be used directly in GitLab CI. Enabling usage and creation  of Docker images for specific tags, environments or branches
                * Container Registry integration with GitLab projects enables automatic creation of Docker images that can be used in GitLab CI by simply pushing Dockerfiles to the repository
                * In conjunction with Dependency Proxy allows to proxy and cache DockerHub
                * Using GitLab Container Registry along with CI/CD strips off the need to authenticate with external docker hub making for less verbose configuration files reusing authentication mechanism
              improving_product_capabilities: |
                * Provide richer metadata of what is in the Docker image in GitLab Container Registry UI
                * Enable creation of rules to protect images as with protected environments to avoid deletion
              link_to_documentation: https://docs.gitlab.com/ee/user/packages/container_registry/
            competitor:
              coverage: 100
              description: JFrog Container Registry resides under  Repositories. Same technology utilized for Packages in Artifactory. Allows configuration of multiple Docker and Helm Registries with fine-grained access control
              details: |
                * JFrog Container Registry documentation links to Artifactory documentation, stating that Container Registry is not a different product or technology but rather a subset of features built on top of Artifactory
                * Image Containers as in the case of Packages/Package Managers, can be queried using [Artifactory Query language](https://www.jfrog.com/confluence/display/JFROG/Artifactory+Query+Language){data-ga-name="link to artifactory query language" data-ga-location="body"} and its docker layers visualized in the UI
                * Allows the creation of Virtual Docker repositories. This type of repositories aggregate images from different repositories (local & remote) and presents it under a single URL
                * JFrog Container Registry documentation link to Artifactory documentation (the main pillar for packages) might lead to confusion in the learning path. As evidenced by a [question](https://stackoverflow.com/questions/58946718/what-is-the-difference-between-jfrog-container-registry-and-jfrog-artifactory#:~:text=Just%20a%20note%20on%20terminology,repositories%20across%20regions%20and%20teams.){data-ga-name="link to what is the difference between jfrog container registry and jfrog artifactory" data-ga-location="body"} on stackoverflow asking for clarification in terminology
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/JFrog+Container+Registry
          - feature: Helm Chart Registry
            gitlab:
              coverage: 50
              projected_coverage: 75
              description: Kubernetes cluster integrations can take advantage of Helm charts to standardize their distribution and install processes. Supporting a built-in helm chart registry allows for better, self-managed container orchestration.
              details: |
                * Helm Packages can use GitLab Package Registry and follow same workflows previously [described](https://docs.gitlab.com/ee/user/packages/helm_repository/#use-cicd-to-publish-a-helm-package){data-ga-name="link to use cicd to publish a helm package" data-ga-location="body"} for publishing and download
              improving_product_capabilities: |
                * Helm chart in the Package Registry is under development. This [epic](https://gitlab.com/groups/gitlab-org/-/epics/6366){data-ga-name="link to helm chart in the package registry is under development" data-ga-location="body"} describes the work in progress to make it production ready
                * Direction [Page](https://about.gitlab.com/direction/package/){data-ga-name="link to direction page" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts
            competitor:
              coverage: 75
              description: Helm Chart Registries are powered by Artifactory. Allow use of local, private and virtual Helm Repositories
              details: |
                * Similarly to Container Registry, being part of Artifactory brings it the possibility to View Helm Chart information in the UI
                * Helm Charts can be deployed using cURL, JFrog CLI and wget
                * There are references in the documentation to a [JFrog Helm Client](https://www.jfrog.com/confluence/display/JFROG/Kubernetes+Helm+Chart+Repositories#KubernetesHelmChartRepositories-UsingtheJFrogHelmClient){data-ga-name="link to jfrog helm client" data-ga-location="body"} which is a fork from the official [Helm repository](https://github.com/JFrog/helm?_gl=1*u3pq9i*_ga*MTE4MDgzNTI2NC4xNjYwMzAyMzk5*_ga_SQ1NR9VTFJ*MTY2MTc2MTUzMi45LjEuMTY2MTc2MjkyNS40Ny4wLjA.){data-ga-name="link to official helm repository" data-ga-location="body"}
                * A key point JFrog emphasizes is the enterprise features available for Helm Charts, such as: HA, repository replication for multi-site development and storage scalability.
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/Kubernetes+Helm+Chart+Repositories#KubernetesHelmChartRepositories-UsingtheJFrogHelmClient
          - feature: Dependency Proxy
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: The GitLab Dependency Proxy can serve as an intermediary between your local developers and automation and the world of packages that need to be fetched from remote repositories. By adding a security and validation layer to a caching proxy, you can ensure reliability, accuracy, and auditability for the packages you depend on.
              details: |
                * GitLab Dependency Proxy currently [supports](https://docs.gitlab.com/ee/user/packages/dependency_proxy/){data-ga-name="link to gitlab dependency proxy" data-ga-location="body"} Docker Images. Reducing Docker hub calls and speeding up pipelines that pull Container images
                * Direction [Page](https://about.gitlab.com/direction/package/){data-ga-name="link to direction page" data-ga-location="body"}
              improving_product_capabilities: |
                * Dependency Proxy [Epic](https://gitlab.com/groups/gitlab-org/-/epics/2920){data-ga-name="link to dependency proxy epic" data-ga-location="body"} to make it complete
              link_to_documentation: https://docs.gitlab.com/ee/user/packages/dependency_proxy/
            competitor:
              coverage: 75
              description: JFrog’s Proxy Repository Caching makes use of Remote Repositories as Caching Proxy for packages and docker images
              details: |
                * Remote Repositories can be combined with Local repositories under a Virtual Repository providing a cache layer for both
                * Requested files are stored in cache layer after being requested, not the entire Remote Repository
                * Certain package types support remote browsing directly from the UI. Docker Hub does not
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/Remote+Repositories#RemoteRepositories-BrowsingRemoteRepositories
          - feature: Dependency Firewall
            gitlab:
              coverage: 0
              projected_coverage: 25
              description:  GitLab ensures that the dependencies stored in your package registries conform to your corporate compliance guidelines. This means you can prevent your organization from using dependencies that are insecure or out of policy.
              details: |
                * Dependency Firewall currently not being offered
                * However, GitLab already provides dependency scanning across a variety of languages to alert users of any known security vulnerabilities
              improving_product_capabilities: |
                * Epic: Make Dependency [Firewall MInimal](https://gitlab.com/groups/gitlab-org/-/epics/5133){data-ga-name="link to make dependency firewall minimal epic" data-ga-location="body"}
              link_to_documentation: https://about.gitlab.com/direction/package/
            competitor:
              coverage: 75
              description: Currently JFrog can't delay updates from Packages if they meet certain criteria that can suggest suspicious activity. Nevertheless using JFrog Xray, already downloaded packages in the repositories can be scanned before they are consumed. If the artifact has vulnerabilities its download is blocked
              details: |
                * Block unscanned: JFrog Xray policy and rules can block unscanned artifacts to be downloaded if these ones haven't been scanned
                * Block Download: Artifactory will block downloads of artifacts that meet preconfigured filter criteria
                * Nexus offers Firewall for Artifactory as a [plugin](https://help.sonatype.com/fw/getting-started/jfrog-artifactory-setup){data-ga-name="link to jfrog artifactory setup" data-ga-location="body"}
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/Creating+Xray+Policies+and+Rules
          - feature: Git LFS
            gitlab:
              coverage: 50
              projected_coverage: 75
              description: Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching.
              details: |
                * GitLab allows managing large files and stores them as a blob either in local disks or remote object storages
                * Compatible and support for Git LFS Client
              improving_product_capabilities: |
                * Enable and configure using GitLab UI
              link_to_documentation: https://docs.gitlab.com/ee/topics/git/lfs/index.html
            competitor:
              coverage: 100
              description: Artifactory supports LFS. Provides an LFS server that works with Git LFS client
              details: |
                * Allows security and access control to LFS Repositories
                * It is possible to configure XRay scans on LFS
                * Supports Git LFS Client to point to JFrog Artifactory
              link_to_documentation: https://www.jfrog.com/confluence/display/JFROG/Git+LFS+Repositories
        overview_analysis: |
          GitLab competes with JFrog’s Pipelines in the Package stage. JFrog is near-complete maturity in most categories in the Package stage, offering a robust solution for Package Management and Software Delivery. However, it lacks the collaboration tools and visibility that is needed when planning features and Source Code Management in a DevOps process. Additionally, JFrog’s offering is not as flexible as GitLab CI. For example, with JFrog it is not possible to create dynamic behaviors.
        gitlab_product_roadmap:
          - roadmap_item: Virtual registries, which will make it easier to migrate customers from JFrog Artifactory to GitLab
          - roadmap_item: Dependency Firewall is a new category of focus, which will help to prevent unknown or unverified providers from introducing potential security vulnerabilities
          - roadmap_item: Improved discoverability and visibility into how dependencies are being used and by whom/which projects
