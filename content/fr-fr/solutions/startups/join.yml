---
  title: Participer au programme GitLab pour les start-up
  description: Inscrivez-vous dès aujourd'hui pour rejoindre le programme GitLab pour les start-up. Notre équipe vous contactera et vous initiera à l'utilisation de GitLab.
  image_alt: GitLab pour les start-up
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          - Programme GitLab pour les start-up
        title: Des logiciels. Plus rapidement.
        subtitle: Réduisez les temps de cycle et déployez plus fréquemment en ménageant vos efforts. En utilisant une seule application pour l'ensemble du cycle de livraison des logiciels, vos équipes peuvent se concentrer sur la livraison de logiciels de meilleure qualité, plus rapidement.
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application-form'
          text: S'inscrire maintenant
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/startups_join.jpeg
          alt: "Image: GitLab pour open source"
          bordered: true
          position: left
    - name: 'startups-intro'
      data:
        title: "Désormais accessible à un plus grand nombre de start-up, un programme conçu pour vous aider à accélérer votre activité."
        subtitle: |

          Les start-up doivent se faire en sorte de connaître une croissance exponentielle tout en faisant plus avec moins. Avec GitLab, vous pouvez compiler des produits innovants pour atteindre de nouveaux clients et parvenir à vos objectifs de revenus.

          **Les start-up éligibles\* bénéficient de GitLab Ultimate gratuitement pendant un an.**

    - name: 'education-case-study-carousel'
      data:
        customers_cta: true
        cta:
          href: '/customers/'
          text: Afficher d'autres témoignages
          data_ga_name: customers
        logo_in_card: true
        case_studies:
          - logo_url: "/nuxt-images/logos/chorus-color.svg"
            institution_name: Te Herenga Waka—Victoria University of Wellington
            img_url: /nuxt-images/blogimages/Chorus_case_study.png
            quote:
              quote_text: La vie est tellement plus facile pour l'ingénierie produit et toutes les personnes qui veulent interagir avec ce domaine, car elles peuvent simplement le faire en utilisant à GitLab.
              author: Russell Levy
              author_title: Cofondateur et directeur technique, Chorus.ai
            case_study_url: /customers/chorus/
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel

    - name: startups-overview
      data:
        header: Qui est éligible au programme GitLab pour les start-up ?
        description: Les éditions GitLab pour les start-up sont adapté à votre stade de développement.
        blocks:
          - header: Start-up en phase de démarrage
            offers:
              - list:
                - |
                  Étape en pré- ou en phase de démarrage : jusqu'à
                  5 millions de dollars de financement.
                - Le financement doit provenir d'une source externe.
                - Nouveaux clients uniquement.
              - title: "Au cours de la première année, vous bénéficierez des éléments suivants\_:"
                list:
                  - Licence Ultimate gratuite pendant un an (GitLab SaaS ou GitLab Auto-géré) jusqu'à 20 sièges.
                  - Assistance non incluse. Pas de panique, GitLab est conçu pour être super facile à utiliser.
              - title: "Au cours de la deuxième année, vous bénéficierez des éléments suivants\_:"
                list:
                  - 50 % de réduction sur toutes les éditions ; jusqu'à 20 utilisateurs.
          - header: Start-up au stade de la première levée de fonds
            offers:
              - list:
                - |
                  Levée de fonds en cours : en série A ou en série B : jusqu'à
                  20 millions de dollars de financement.
                - Le financement doit provenir d'une source externe.
                - Nouveaux clients uniquement.
              - title: "Au cours de la première année, vous bénéficierez des éléments suivants\_:"
                list:
                  - 50 % de réduction sur toutes les éditions ; jusqu'à 20 utilisateurs.
              - title: "Au cours de la deuxième année, vous bénéficierez des éléments suivants\_:"
                list:
                  - 25 % de réduction sur toutes les éditions ; jusqu'à 20 utilisateurs.
        information:
          header: Exigences et informations supplémentaires
          list:
            - Une fois que votre inscription au programme effectué, il vous sera peut-être demandé de fournir des informations supplémentaires afin de vérifier votre éligibilité.
            - Une fois votre éligibilité au programme GitLab pour les start-up acceptée, en tant que membre du programme, vous serez soumis à l'[accord d'abonnement GitLab](/handbook/legal/subscription-agreement/).
            - Pour les instances auto-hébergées, les utilisateurs doivent activer les fonctionnalités d'utilisation et d'analyse.
            - Le programme GitLab pour les start-up est proposé à la seule discrétion de GitLab et est susceptible d'être modifié.
            - Des questions ? Contactez-nous à l'adresse [startups@gitlab.com](mailto:startups@gitlab.com).
    - name: 'open-source-form-section'
      data:
        id: application-form
        title: Formulaire d'inscription
        blocks:
          - content: |
              Inscrivez-vous dès aujourd'hui pour rejoindre le programme GitLab pour les start-up. Notre équipe vous contactera et vous initiera à l'utilisation de GitLab. Nous sommes ravis de découvrir les produits innovants que vous construisez avec notre assistance.
        form:
          required_text: Veuillez remplir tous les champs.
          formId: 4066
          form_header: ''
          submitted_message:
            header: Merci pour votre inscription !
            body: Vous allez recevoir un courriel de confirmation sous peu.
          datalayer: startups
    - name: 'startups-link'
      data:
        header: Pourquoi rejoindre le programme GitLab pour les start-up ?
        description: Du capital pour démarrer votre entreprise au flux de trésorerie d'exploitation positif, en passant par l'évolution vers le statut de grande entreprise multinationale, quelles que soient vos ambitions, GitLab se tient prêt à évoluer à vos côtés, à mesure que votre entreprise se développe.
        button:
          href: /solutions/startups
          text: En savoir plus
        image: /nuxt-images/solutions/startups_hero.jpeg
        alt: une foule de participants lors d'une présentation
        icon: calendar-alt-2
    - name: faq
      data:
        header: FAQ
        show_all_button: true
        background: true
        jump_down_butoon_text: Sauter
        texts:
          hide: Cacher tout
          show: Afficher tout
        groups:
        - header: ""
          questions:
            - question: Les start-up autofinancées son-elles éligibles ?
              answer: >-
                Le programme est actuellement disponible pour les entreprises recevant un financement d'une source extérieure uniquement et répondant aux paramètres de qualification donnés. Lors des itérations ultérieures, nous envisagerons d'évaluer d'autres portefeuilles.
            - question: Comment le financement externe est-il validé ?
              answer: >-
                Les données de candidature seront validées par GitLab pour déterminer l'éligibilité. Les candidats doivent fournir un profil Crunchbase, Pitchbook, leurs états financiers ou toute autre justification raisonnable pour prouver le statut de leur financement.
            - question: Que se passera-t-il au bout d'un an ?
              answer: |
                Veuillez vous référer au positionnement de votre start-up dans les paramètres de financement pour vous assurer de connaître le tarif qui sera appliqué dès la 2e année. Un membre de notre équipe vous contactera bien à l'avance pour vous fournir des conseils et le temps nécessaire pour vous préparer.
            - question: Qui est comptabilisé dans l'abonnement ?
              answer: >-
                Veuillez consulter la réponse détaillée dans notre [FAQ dédiée aux licences et abonnements](/pricing/licensing-faq/#who-gets-counted-in-the-subscription)
            - question: A-t-on le droit d'augmenter le nombre de sièges par la suite ?
              answer: >-
                 Si vous souhaitez augmenter le nombre de sièges de votre licence actuelle, veuillez envoyer un courriel à l'adresse [startups@gitlab.com](mailto:startups@gitlab.com) ,et nous vous établirons un devis pour les sièges supplémentaires.
