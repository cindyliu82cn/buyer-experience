---
  title: GitLab CI/CD pour GitHub
  description: Avec l'intégration de GitHub, les utilisateurs de GitLab peuvent désormais créer un projet CI/CD dans GitLab connecté à un dépôt de code externe de GitHub.com ou GitHub Enterprise !
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab CI/CD pour GitHub
        subtitle: Hébergez votre code sur GitHub. Compilez, testez et déployez sur GitLab.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html
          text: Documentation
          data_ga_name: github integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: GitLab CI/CD pour GitHub"
    - name: copy
      data:
        block:
          - header: Automatisation de la compilation et des tests
            id: automate-build-and-test
            text: |
              Avec l'intégration de GitHub, les utilisateurs de GitLab peuvent désormais créer un projet CI/CD dans GitLab connecté à un dépôt de code externe de GitHub.com ou GitHub Enterprise. Cela invitera automatiquement GitLab CI/CD à s'exécuter chaque fois que le code est poussé vers GitHub et à publier les résultats CI/CD à la fois sur GitLab et sur GitHub une fois le processus terminé.
    - name: copy-media
      data:
        block:
          - header: À qui s'adresse GitLab CI/CD pour GitHub ?
            id: who-is-git-lab-ci-cd-for-git-hub-for
            text: |
              ### Open source projects
              Si vous avez un projet public et open source sur GitHub, vous pouvez maintenant profiter du processus CI/CD gratuitement sur GitLab.com. Dans le cadre de notre engagement en faveur de l'open source, nous offrons gratuitement à tous les projets publics les fonctionnalités de notre édition la plus complète (GitLab SaaS Ultimate). Alors que d'autres fournisseurs de solutions CI/CD vous limitent à exécuter quelques jobs simultanés, [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="open source projects CI"} offre aux projets open source la possibilité d'exécuter des centaines de jobs simultanés avec 50 000 minutes de calcul gratuites.

              ### Large Enterprises
              Lorsque nous discutons avec nos plus gros clients, ils nous expliquent que bien souvent leur approche CI/CD est réalisée par de nombreuses équipes qui utilisent un grand nombre d'outils différents. Ils veulent que l'adoption de GitLab pour le processus CI/CD deviennent la norme, mais le code est stocké dans GitLab, GitHub et d'autres dépôts à la fois. Cette nouvelle fonctionnalité de GitLab permet donc désormais aux entreprises d'utiliser des pipelines CI/CD communs à tous leurs différents dépôts. Ces clients sont pour nous une audience clé et c'est pour cette raison que nous avons intégré le CI/CD pour GitHub dans notre forfait Premium de GitLab Auto-géré.

              ### Anyone using GitHub&#46;com
              Bien que GitLab soit conçu pour utiliser le SCM et le CI/CD dans la même application, nous comprenons l'intérêt d'utiliser GitLab CI/CD avec le contrôle de version GitHub. L'année prochaine, notre fonctionnalité GitLab CI/CD pour GitHub sera intégrée à notre édition gratuite [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="anyone using github.com"}. Ainsi, toute personne utilisant GitHub, pour des projets personnels, ou dans le cadre d'une start-up ou d'une PME, peut utiliser GitLab CI/CD gratuitement. Avec un seuil de départ de 400 minutes de calcul gratuites, les utilisateurs peuvent également [ajouter leurs propres runners](https://docs.gitlab.com/ee/ci/runners/#registering-a-specific-runner){data-ga-name="add runners" data-ga-location="body"} ou passer à un forfait d'un niveau supérieur afin d'augmenter leur nombre de minutes de calcul.

              ### Gemnasium customers
              Nous avons récemment [acquis Gemnasium](/press/releases/2018-01-30-gemnasium-acquisition.html){data-ga-name="gemnasium" data-ga-location="body"}. Bien que nous soyons très enthousiastes à l'idée de voir une équipe aussi formidable rejoindre nos rangs, nous souhaitons également accompagner les personnes qui utilisaient Gemnasium dans leur parcours de migration. Nous avons déjà [incorporé les fonctionnalités de Gemnasium](/releases/2018/02/22/gitlab-10-5-released/#gemnasium-dependency-checks){data-ga-name="gemnasium features" data-ga-location="body"} dans le cadre de notre analyse de sécurité intégrée. GitLab CI/CD pour GitHub permet désormais aux clients Gemnasium qui utilisaient GitHub + Gemnasium de commencer à utiliser GitLab CI/CD pour leurs besoins de sécurité sans avoir à migrer leur code.
            image:
              image_url: /nuxt-images/logos/github-logo.svg
              alt: ""
          - header: Avantages
            id: benefits
            inverted: true
            text: |
              Avec GitLab CI/CD pour GitHub, les utilisateurs peuvent créer un projet CI/CD dans GitLab connecté à un dépôt de code externe de GitHub. Ce projet configurera automatiquement plusieurs composants :

              * [Mise en miroir en mode « tirage »](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository){data-ga-name="pull mirroring"data-ga-location="body"} du dépôt.
              * Un crochet Web poussé vers GitLab déclenche le processus CI/CD immédiatement dès qu'un code est validé.
              * Renvoi du statut CI des crochets Web d'intégration du service de projet GitHub à GitHub.
            image:
              image_url: /nuxt-images/features/github-status-github.png
              alt: ""
          - header: GitLab CI/CD pour les dépôts externes
            text: |
              Non seulement GitLab s'intègre à GitHub, mais vous pouvez également exécuter le processus CI/CD à partir de n'importe quel dépôt git externe de n'importe quel fournisseur en ajoutant un dépôt par URL à votre projet et en configurant le crochet Web. Par exemple, vous pouvez [configurer Bitbucket pour qu'il utilise GitLab CI/CD](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/bitbucket_integration.html){data-ga-name="bitbucket for gitlab" data-ga-location="body"}.

              Consultez la documentation dédiée à [GitLab CI/CD pour les dépôts externes](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/){data-ga-name="ci/cd for external repositories" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/git-logo.svg
              alt: ""
          - header: Forfaits et tarifs
            id: plans-and-pricing
            inverted: true
            text: |
              GitLab CI/CD pour GitHub n'est pas facturé séparément, mais est fourni empaqueté en tant que fonctionnalité du produit standard complet GitLab.

              Pour les **installations de l'édition GitLab Autogéré**, GitLab CI/CD pour GitHub est disponible pour les clients disposant d'un forfait de licence **Premium** ou **Ultimate**.

              .GitLab CI/CD pour GitHub était proposé dans notre édition **GitLab Gratuit** jusqu'au 22 mars 2020 dans le cadre d'une campagne de promotion. (Depuis le 22 mars 2020, cette fonctionnalité a été intégrée à l'édition **Premium** et est disponible pour les utilisateurs possédant un forfait **Premium** ou **Ultimate**.)

              Pour en savoir plus sur les options d'abonnement à GitLab, consultez la [page des tarifs](/pricing/){data-ga-name="pricing" data-ga-location="body"}.
            icon:
              name: checklist
              alt: Liste de contrôle
              variant: marketing
              hex_color: '#F43012'
          - header: En savoir plus
            id: learn-more
            text: |
              * GitLab CI/CD : découvrez les [avantages de GitLab CI/CD](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}.
              * Documentation : lancez-vous en vous aidant de la [documentation](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html){data-ga-name="documentation" data-ga-location="body"}.
              * Release : lisez l'article à propos de la release [GitLab 10.6](/releases/2018/03/22/gitlab-10-6-released/){data-ga-name="gitlab 10.6" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/gitlab-logo.svg
              alt: ""
