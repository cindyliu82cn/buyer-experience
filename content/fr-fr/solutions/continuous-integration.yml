template: feature
title: 'Intégration et livraison continues'
description: 'Mettez en place une livraison de logiciels reproductible et à la demande'
components:
  feature-block-hero:
    data:
      title: Intégration et livraison continues
      subtitle: Mettez en place une livraison de logiciels reproductible et à la demande
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: Commencer votre essai gratuit
        url: /free-trial/
      image:
        image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
        hide_in_mobile: vrai
        alt: "Image\_: boucle infinie GitLab"
  side-navigation:
    links:
      - title: Présentation
        href: '#overview'
      - title: Avantages
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: L'intégration et la livraison continues de GitLab
          content_summary: >-
            automatisent toutes les étapes nécessaires pour créer, tester et déployer votre code dans votre environnement de production. <br><br> <b>L'intégration continue</b> automatise les compilations, fournit des commentaires lors de la revue du code et automatise les tests de qualité et de sécurité du code. Elle crée un paquet de versions de release qui peut être déployé dans votre environnement de production. <br><br> <b>La livraison continue fournit</b> automatiquement l'infrastructure, gère les modifications de l'infrastructure, les tickets et le contrôle des versions de release. Elle permet le déploiement progressif du code, la vérification et le suivi des modifications apportées. Elle offre également la possibilité de restaurer un déploiement antérieur si nécessaire. Ensemble, l'intégration et la livraison continues de GitLab vous aident à automatiser le cycle de vie de votre développement logiciel, ce qui le rend reproductible à la demande, tout en ne demandant qu'une intervention manuelle minimale.
          content_image: /nuxt-images/resources/resources_20.jpg
          content_heading: Automatisez des flux de travail entiers avec GitLab CI/CD
          content_list:
            - Améliorez la productivité des développeurs grâce aux résultats de tests en contexte.
            - Créez des versions de release sécurisées et conformes en testant chaque modification.
            - Mettez en place des protections pour vos déploiements.
            - Automatisez des pipelines cohérents pour plus de simplicité et d'évolutivité.
            - Automatisez à la fois les applications et l'infrastructure.
            - Accélérez votre parcours cloud-native et Cloud hybride.
      feature-benefit:
        data:
          feature_heading: Automatisation complète
          feature_cards:
            - feature_name: Tests en contexte
              icon:
                name: automated-code
                variant: marketing
                alt: Icône de code automatisé
              feature_description: >-
                Chaque modification déclenche un pipeline automatisé pour vous faciliter le travail.
              feature_list:
                - Testez tout, que ce soit le code, les performances, la charge ou la sécurité.
                - Résultats partagés dans la requête de fusion avant que le développeur ne change de tâche.
                - Vérifiez, collaborez, itérez et approuvez en contexte.
            - feature_name: Sécurisé et conforme
              icon:
                name: release
                variant: marketing
                alt: Icône de bouclier avec une coche
              feature_description: >-
                Des pipelines de conformité à l'analyse de sécurité intégrée, consultez toutes les informations au même endroit pour bénéficier d'une visibilité accrue et d'un meilleur contrôle.
              feature_list:
                - Tests de sécurité des applications, intégrés au flux de travail des développeurs.
                - Gestion des vulnérabilités pour les professionnels de la sécurité.
                - Approbations, rapports d'audit et traçabilité pour simplifier la conformité aux politiques.
            - feature_name: Mettez en place des protections pour vos déploiements.
              icon:
                name: approve-dismiss
                variant: marketing
                alt: Icône approuver et rejeter
              feature_description: >-
                Évitez les déploiements échoués ou non performants en mettant en place des stratégies pour protéger vos déploiements.
              feature_list:
                - Pipelines de conformité pour garantir que les politiques sont suivies de manière cohérente.
                - Contrôle granulaire du code à déployer grâce aux feature flags.
                - Décidez vers qui déployer à l'aide des déploiements progressifs, canari et bleu-vert.
            - feature_name: Des pipelines conçus pour plus de simplicité et d'évolutivité
              icon:
                name: pipeline-alt
                variant: marketing
                alt: Icône de pipeline
              feature_description: >-
                Des pipelines flexibles pour vous accompagner à toutes les étapes de votre parcours.
              feature_list:
                - Des modèles intégrés pour vous aider à démarrer facilement.
                - Création automatique de pipelines via Auto DevOps.
                - Évoluez avec les pipelines parent-enfant et les trains de fusion.
            - feature_name: Automatisation des applications et de l'infrastructure
              icon:
                name: devsecops
                variant: marketing
                alt: Icône de boucle DevSecOps
              feature_description: >-
                Une seule plateforme pour automatiser les applications et les infrastructures.
              feature_list:
                - Minimisez les coûts de licence et la courbe d'apprentissage.
                - Tirez parti des bonnes pratiques de DevSecOps en matière d'application pour l'infrastructure.
            - feature_name: Accélération de votre parcours cloud-native et Cloud hybride
              icon:
                name: continuous-integration
                variant: marketing
                alt: Icône d'intégration continue
              feature_description: >-
                GitLab est un DevSecOps conçu pour le multicloud, qui fonctionne avec toutes les infrastructures.
              feature_list:
                - Prend en charge le déploiement sur des machines virtuelles, des clusters Kubernetes ou le FaaS de différents fournisseurs de Cloud.
                - Prend en charge Amazon Web Services, Google Cloud Platform, Microsoft Azure ou votre propre Cloud privé.
  group-buttons:
    data:
      header:
        text: Explorez d'autres solutions de GitLab pour l'intégration continue.
        link:
          text: Explorer d'autres solutions
          href: /solutions/
      buttons:
        - text: Automatisation de la livraison
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: Sécurité logicielle continue
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
        - text: Conformité
          icon_left: shield-check
          href: /solutions/compliance/
  report-cta:
    layout: "dark"
    title: Rapports d'analystes
    reports:
    - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lire le rapport
    - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
      url: /gartner-magic-quadrant/
      link_text: Lire le rapport
  solutions-resource-cards:
    data:
      title: Ressources
      link:
        text: Afficher toutes les ressources
      column_size: 4
      cards:
        - icon:
            name: ebook
            variant: marketing
            alt: Icône e-book
          event_type: Livre électronique
          header: Comment convaincre vos dirigeants d'adopter l'approche CI/CD
          link_text: En savoir plus
          image: "/nuxt-images/resources/resources_1.jpeg"
          href: https://page.gitlab.com/2021_eBook_leadershipCICD.html
          data_ga_name: Comment convaincre vos dirigeants d'adopter l'approche CI/CD
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: Icône d'étude de cas
          event_type: Étude de cas
          header: >-
            Cinq clients AWS qui s'appuient sur GitLab pour leur flux de travail DevSecOps
          link_text: En savoir plus
          href: https://about.gitlab.com/resources/ebook-five-aws-customers/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: Five AWS customers who depend on GitLab for DevSecOps workflow
          data_ga_location: body
        - icon:
            name: webcast
            variant: marketing
            alt: Icône de webcast
          event_type: Diffusion sur le Web
          header: 7 astuces CI/CD de GitLab
          link_text: Regarder maintenant
          href: https://about.gitlab.com/webcast/7cicd-hacks/
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: 7 GitLab CI/CD hacks
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: Icône de vidéo
          event_type: Vidéo
          header: Démarrer avec GitLab CI/CD
          link_text: Regarder maintenant
          href: https://www.youtube.com/embed/sIegJaLy2ug
          image: /nuxt-images/features/resources/resources_webcast.png
          data_ga_name: Getting started with GitLab CI/CD
          data_ga_location: body
        - icon:
            name: video
            variant: marketing
            alt: Icône de vidéo
          event_type: Vidéo
          header: Vue d'ensemble CI/CD
          link_text: Regarder maintenant
          href: https://www.youtube.com/embed/l5705U8s_nQ?start=397
          image: /nuxt-images/resources/fallback/img-fallback-cards-cicd.png
          data_ga_name: CI/CD Overview
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: Icône d'étude de cas
          event_type: Étude de cas
          header: Évolution automatique pour accélérer les pipelines
          link_text: En savoir plus
          href: https://about.gitlab.com/customers/EAB/
          image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
          data_ga_name: Auto-scaling to achieve faster pipelines
          data_ga_location: body