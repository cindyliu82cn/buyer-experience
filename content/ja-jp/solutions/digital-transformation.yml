---
  title: デジタルトランスフォーメーションを加速する方法
  description: GitLabを活用したデジタルトランスフォーメーションについて学びましょう。
  components:
    - name: 'solutions-hero'
      data:
        title: デジタルトランスフォーメーションを加速
        subtitle:  ソフトウェアデリバリーをスピードアップすることで、デジタルトランスフォーメーションが加速します。Forrester社の業界リーダーから、チームのトランスフォーメーションを進める方法を学びましょう。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: ウェビナーを視聴
          url: /webcast/justcommit-reduce-cycle-time/
          data_ga_name: software delivery webinar
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像：GitLabでデジタルトランスフォーメーションを加速"
    - name: 'copy-media'
      data:
        block:
          - header: デジタルトランスフォーメーションのメリット
            miscellaneous: |

              デジタルトランスフォーメーションとは、イノベーションと効率化の促進、チームの働き方の改善、そして競合他社を飛躍的に上回る新たなビジネスチャンスを創出することであり、さらにこれらすべてを通じて、新しく刷新された利用体験を顧客にお届けするアプローチを指します。

              すべての業界は、変革期を迎えています。顧客が求める基準は高まっており、たったワンクリックで競合他社のページに移動できてしまうため、顧客を維持することがますます難しくなっています。業界に関係なく、予期せぬところから競争が生まれるようになり、サービスの最前線および中心にテクノロジーを据えることが必要になっています。実際の例を挙げると、世界最大のタクシー会社は車を所有していませ。また、世界最大の宿泊施設提供企業は物件を所有しておらず、世界最大のコンテンツネットワークもまた、コンテンツを所有していません。

              デジタルトランスフォーメーションは、もはやできれば実現したいものではなく、必要不可欠なものになりました。
            metadata:
              id_tag: why-digital-transformation
          - header: 成功に導く
            miscellaneous: |
                デジタルトランスフォーメーションは、既存のビジネスモデルと運用モデルのディスラプション（破壊）をもたらし、業界を再編しています。現在において大事なのは、デジタルトランスフォーメーションを実施するかどうかではなく、競争力を維持するためにいかに早く実施できるかということです。ITのモダナイゼーションは、デジタルトランスフォーメーションを実現するにあたって重要なステップになりますが、鍵となるのは組織全体にわたってデジタルカルチャーを構築することです。デジタルカルチャーの構築に失敗すると、こうしたイニシアチブの成功が危ぶまれてしまいます。[McKinsey社](https://www.mckinsey.com/industries/retail/our-insights/the-how-of-transformation)によると、なんと「移行プログラム」の70%において、設定された目標を達成できないことがわかっています。

                では、組織はどうすれば、こうしたイニシアチブを成功に導けるのでしょうか？[McKinsey社の調査](https://www.mckinsey.com/business-functions/people-and-organizational-performance/our-insights/unlocking-success-in-digital-transformations)では、成功のメトリクスを次の5つの主要カテゴリに分類しています。

                * デジタル分野に精通する最適なリーダーを配置
                * 将来的に従業員に必要となる能力を育成
                * 従業員が新たな働き方を取り入れられるように支援
                * 日常的に使用するツールをデジタル化してアップグレード
                * 従来の方法とデジタルな方法を併用して積極的なコミュニケーションを実現

                GitLabを使用すると、デジタルトランスフォーメーションの成功要因とされる5つの主要なメトリクスのうち4つを達成しやすくなります。実際に、GitLab内では[こちらのページで紹介する原則に則って](/company/culture/all-remote/){data-ga-name="all remote" data-ga-location="body"}、完全リモートの職場と共同作業環境を実現しています。
            metadata:
              id_tag: unlocking-success
          - header: トランスフォーメーションのステップ
            miscellaneous: |

                デジタルトランスフォーメーションの最初のステップは、デジタルイニシアチブにおける目標を特定することです。[Gartner社によると](gartner.com/smarterwithgartner/cio-agenda-2019-digital-maturity-reaches-a-tipping-point)、デジタルイニシアチブに着手している組織は96%にのぼり、デジタルトランスフォーメーションは今や主流となり、ほとんどのCIO（最高情報責任者）が最重要事項として捉えています。GitLabは、規模の異なる何千もの組織とのやり取りの中で、組織は主に次の3つの目標を達成するためにデジタルトランスフォーメーションに着手していることを特定しました。

                **運用効率の向上**

                組織がマニュアルで反復的に行う作業から自動化された統合ソリューションに移行して運用効率と開発者の満足度を高めるという目標の下、デジタルトランスフォーメーションに着手するにあたり、主な原動力となっているのは効率性の向上です。

                **より優れた製品の迅速なデリバリー**

                競合他社に対抗して打ち勝つために、企業は顧客維持と新規顧客の獲得を目標に、デジタルトランスフォーメーションを活用してより充実した顧客体験を創造しています。これを実現するために、組織は、コラボレーションを伴わないサイロ化されたプロセスから、統合プロセスに移行する方法を模索しています。統合プロセスに移行することで、チーム間のコミュニケーションの改善、待機時間や引き継ぎの回避につながり、顧客に対してより迅速に製品を展開できます。

                **セキュリティとコンプライアンスに関するリスク低減**

                IDC社が2019年に大企業を対象に実施したDevOpsに関するアンケート調査によると、セキュリティとガバナンスは組織におけるトップ3の課題の1つであり、今後2〜3年間で最も投資すべき分野であることが明らかになりました。セキュリティエクスポージャーのリスク低減、セキュリティによる混乱の軽減、監査の合理化といったニーズが、組織をデジタルトランスフォーメーションへの投資に駆り立てています。

                原動力となるニーズを特定できたら、成功に目を向けることが重要です。それぞれの目標に対して、具体的かつ測定可能なビジネス成果を定めて、実施前と実施後のシナリオを示す必要があります。
            metadata:
              id_tag: path-to-transformation
          - header: 成功の測定
            metadata:
              id_tag: measuring-success
            miscellaneous: |

              デジタルトランスフォーメーションのメトリクスは、ほとんどの組織にとって悩みの種となっています。その理由は、さまざまなメトリクスが散在しているか、一般的なメトリクスであっても組織には関連性のないものであるためです。適切なメトリクスを特定できなければ、デジタルトランスフォーメーションの成果を測定することは非常に困難になります。

              GitLabは、目標に関して組織とやり取りする中で、目標に基づく成功の測定に関連すると考えられる主要なビジネスメトリクスをいくつか特定しました。もちろん、以下のメトリクスがすべてではなく、組織のニーズに応じて調整する必要があります。

              **運用効率の向上**

                * ツールチェーンの所有権にかかる総コスト（ライセンス、フルタイム当量、コンピューティングコスト）
                * サイクルタイム
                * デプロイ頻度
                * 開発者の離職率
                * 従業員満足度



              **より優れた製品の迅速なデリバリー**

                * 収益
                * 顧客数
                * 市場占有率
                * 納期どおりのリリースおよび予算以内のリリースの割合
                * サイクルタイム（ステージあたりの所要時間と待機時間、開発者あたりのスループット）
                * 平均修復時間（MTTR）



              **セキュリティとコンプライアンスに関するリスク低減**

                * スキャン済みコードの割合
                * 開発時に対処できなかった致命的なセキュリティの脆弱性の割合
                * 監査の通過率と実際の所要時間
                * セキュリティの脆弱性の平均修復時間（MTTR）

          - header: GitLabにお任せください
            metadata:
              id_tag: gitlab-can-help
            miscellaneous: |

              最も包括的なDevSecOpsプラットフォームであるGitLabを使用すれば、上記のデジタルトランスフォーメーションの目標を達成できます。GitLabは、プラグインを廃止し、インテグレーションの複雑性を排除することで、ソフトウェアデリバリーのツールチェーンを簡素化し、チームの本業である「優れたソフトウェアの展開」に専念できるようにサポートします。

              GitLabは、ソフトウェア開発ライフサイクル全体（着想から本番稼働まで）にわたってユーザーを支援します。GitLabは、サイロ化やインテグレーション関連の問題の原因となる一連の手順や引き継ぎごとに作業やツールを整理するのではなく、組織全体のコラボレーションを活発化させ、DevSecOpsライフサイクル全体のワークフロー、プロセス、セキュリティ、コンプライアンスを可視化します。

              デジタルトランスフォーメーションにおいてGitLabがどのように役立つかについて、以下のページで詳細をお読みください。

              [バリューストリーム管理](https://about.gitlab.com/solutions/value-stream-management)と[DORAメトリクス](https://about.gitlab.com/solutions/value-stream-management/dora/)によるエンジニアリングチームの生産性向上

              [GitLabの顧客事例](/customers/){data-ga-name="customers" data-ga-location="body"}

              [GitLabに関するアナリストの評価](/analysts/){data-ga-name="analysts" data-ga-location="body"}

              GitLabに関するリソースをほかにもお探しの場合は、[ドキュメントにアクセス](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"}するか[ブログを参照](/blog/){data-ga-name="blog" data-ga-location="body"}してください。
    - name: copy-resources
      data:
        block:
            - metadata:
                id_tag: resources
              text: |

                デジタルトランスフォーメーションに関するリソースで、特に参考になりそうなものをご紹介します。書籍やブログ、動画、ポッドキャストなど、アプローチの定義や実施に関して貴重なインサイトを得られるおすすめのリソースがありましたら、ぜひお聞かせください。
                お気に入りのリソースがあれば、ぜひTwitterでGitLab（[@GitLab](https://twitter.com/gitlab)）をメンションして共有してください！
              resources:
                report:
                  header: レポート
                  links:
                    - text: デジタルトランスフォーメーションを成功に導く（McKinsey社）
                      link: https://www.mckinsey.com/business-functions/people-and-organizational-performance/our-insights/unlocking-success-in-digital-transformations
                    - text: 世界経済フォーラムのデジタルトランスフォーメーションに対するイニシアチブ（世界経済フォーラム）
                      link: https://www3.weforum.org/docs/DTI_Maximizing_Return_Digital_WP.pdf
                    - text: 成功の確率を高める（McKinsey社）
                      link: https://www.mckinsey.com/business-functions/mckinsey-digital/our-insights/digital-transformation-improving-the-odds-of-success
                    - text: デジタルトランスフォーメーションの進捗の測定方法（Gartner社）
                      link: https://www.gartner.com/smarterwithgartner/how-to-measure-digital-transformation-progress