---
  title: 継続的なソフトウェアセキュリティ
  description: GitLabなら、セキュリティをDevSecOpsライフサイクルに簡単に統合できます。セキュリティとコンプライアンスはすぐに使えるように組み込まれており、ソフトウェアの整合性を守るために必要な可視性とコントロールを提供します。
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: 継続的なソフトウェアセキュリティ
        subtitle: ビルトインのDevSecOpsでセキュリティをシフトレフトします。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: GitLabの無限ループ"
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: 概要
          href: '#overview'
        - title: メリット
          href: '#benefits'
        - title: 機能
          href: '#capabilities'
        - title: 価格設定
          href: "#pricing"
        - title: 事例
          href: '#case-studies'
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                text:
                  highlight: GitLabなら、セキュリティをDevSecOpsライフサイクルに簡単に統合できます。
                  description: セキュリティとコンプライアンスはすぐに使えるように組み込まれており、ソフトウェアの整合性を守るために必要な可視性や制御を提供します。
            - name: by-solution-benefits
              data:
                title: Security. Compliance. Built-in.
                image:
                  image_url: "/nuxt-images/resources/resources_11.jpeg"
                  alt: ノートパソコンでの作業
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: 統合されたテストと修正
                    text: コード変更を確定するたびに、GitLabは実用的なセキュリティとコンプライアンスの[調査結果を開発者]に提供(https://docs.gitlab.com/ee/user/application_security/)するので、開発者がまだコードを作成しているライフサイクルの早い段階で、修正できます。
                  - icon:
                      name: continuous-integration
                      alt: 継続的インテグレーションアイコン
                      variant: marketing
                    header: ソフトウェアの脆弱性管理
                    text: さらに、解決に至るまで、セキュリティ担当者が[残りの脆弱性を管理](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center)できるよう支援します。
                  - icon:
                      name: cloud-tick
                      alt: 継続的インテグレーションのアイコン
                      variant: marketing
                    header: クラウドネイティブアプリケーションのセキュリティ
                    text: GitLabは、クラウドネイティブアプリケーションと、コンテナやInfrastructure as Code、APIなど、そのアプリケーションが依存するインフラストラクチャを保護します。
                  - icon:
                      name: automated-code
                      alt: 自動コードアイコン
                      variant: marketing
                    header: 制御とポリシーの自動化
                    text: GitLabのコンプライアンスに準拠したパイプライン、MR承認、監査イベントのエンドツーエンドの透明性、およびビルトインされた[さまざまな制御](https://docs.gitlab.com/ee/administration/compliance.html)は、ソフトウェアサプライチェーンを保護し、[コンプライアンスのニーズ](https://about.gitlab.com/solutions/compliance/)に対応できます。
        - name: 'by-solution-value-prop'
          id: benefits
          data:
            title: 開発者がスピーディかつ安全に実行できる運用を構築
            cards:
              - title: シンプルさ
                description: 包括的なアプリケーションセキュリティを備えた単一のプラットフォームを単一価格でご利用いただけます。
                list:
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/" data-ga-name="application security testing" data-ga-location="body">アプリケーションセキュリティテスト</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/vulnerability_report/" data-ga-name="vulnerability management" data-ga-location="body">脆弱性管理</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/container_scanning/" data-ga-name="deployed images" data-ga-location="body">デプロイイメージのスキャン</a>
                icon:
                  name: release
                  alt: アジャイルアイコン
                  variant: marketing
              - title: 可視化
                description: 誰が何を、どこで、いつ変更したかを最初から最後まで確認できます。
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_events.html" data-ga-name="audit events" data-ga-location="body">監査イベント</a>
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_reports.html" data-ga-name="audit reports" data-ga-location="body">監査レポート</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/dependency_list/#dependency-list" data-ga-name="dependency list" data-ga-location="body">依存関係リスト(BOM)</a>
                icon:
                  name: magnifying-glass-code
                  alt: コードを虫眼鏡で拡大するアイコン
                  variant: marketing
              - title: 制御
                description: コンプライアンスフレームワークは、一貫性があり、一般的な制御やポリシーの自動化を行います
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/compliance.html" data-ga-name="compliance capabilites" data-ga-location="body">一般的なコンプライアンス制御</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/configuration/" data-ga-name="security policy configuration" data-ga-location="body">セキュリティポリシーの設定</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration" data-ga-name="compliant pipelines" data-ga-location="body">コンプライアンスパイプライン</a>
                icon:
                  name: less-risk
                  alt: 低リスクアイコン
                  variant: marketing
        - name: 'home-solutions-container'
          id: capabilities
          data:
            title: DevSecOpsプラットフォームのセキュリティ
            image: /nuxt-images/home/solutions/solutions-top-down.png
            alt: "オフィスにおけるトップダウンの画像"
            description: GitLabトラストセンターにアクセスして、[GitLabソフトウェア](https://about.gitlab.com/security/)をどのように保護し、業界標準に準拠しているかをご確認ください。
            white_bg: true
            animation_type: fade
            solutions:
              - title: CIパイプライン内のテスト
                description: テストにはお使いのスキャナー、またはGitLabが提供するスキャナーを使用できます。セキュリティをシフトレフトして、開発者が発生したばかりのセキュリティの欠陥を見つけて修正できるようにします。包括的なスキャナーには、SAST、DAST、秘密、依存関係、コンテナ、IaC、API、クラスターイメージ、およびファジングテストが含まれます。
                link_text: 詳細はこちら
                link_url: https://docs.gitlab.com/ee/user/application_security/
                data_ga_name: Test CI pipeline
                data_ga_location: body
                icon:
                  name: pipeline-alt
                  alt: パイプラインのアイコン
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_1.png
                alt: "チーム間のやり取りを表現したふきだし"
              - title: 依存関係の評価
                description: 依存関係とコンテナをスキャンして、セキュリティ上の欠陥を見つけます。インベントリの依存関係が使用されます。
                icon:
                  name: visibility
                  alt: 可視化アイコン
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_2.png
                alt: "チーム間のやり取りを表現したふきだし"
              - title: クラウドネイティブアプリの保護
                description: クラウドネイティブなInfrastructure as Code、API、クラスターイメージなどのセキュリティをテストします。
                icon:
                  name: cloud-tick
                  alt: チェックマーク付きの雲のアイコン
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_3.png
                alt: "チーム間のやり取りを表現したふきだし"
              - title: 脆弱性の管理
                description: セキュリティ担当者が、パイプライン、オンデマンドスキャン、サードパーティ、バグバウンティによって、ソフトウェアの脆弱性をすべて1か所で調査し、トリアージし、管理できるように設計されています。脆弱性がマージされると、すぐに可視化されます。解決に向け、より簡単にコラボレーションできます。
                icon:
                  name: continuous-integration
                  alt: 継続的インテグレーションアイコン
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_4.png
                alt: "チーム間のやり取りを表現したふきだし"
              - title: Secure your software supply chain
                description: ソフトウェアの開発ライフサイクル全体にわたって、セキュリティおよびコンプライアンスポリシーを自動化します。コンプライアンスに準拠したパイプラインによって、パイプラインポリシーが回避されないようにし、一般的な制御によってエンドツーエンドの制御機能を提供します。
                icon:
                  name: shield-check
                  alt: チェックマーク付きの盾アイコン
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_5.png
                alt: "チーム間のやり取りを表現したふきだし"
        - name: 'tier-block'
          class: 'slp-mt-96'
          id: 'pricing'
          data:
            header: 最適なプランを見つけましょう
            tiers:
              - id: free
                title: Free
                items:
                  - 静的アプリケーションセキュリティテスト(SAST)と、シークレット検出
                  - JSONファイルの調査
                link:
                  href: /pricing/
                  text: 詳細はこちら
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: Freeプラン
              - id: premium
                title: Premium
                items:
                  - 静的アプリケーションセキュリティテスト(SAST)と、シークレット検出
                  - JSONファイルの調査
                  - MRの承認とその他の一般的な制御
                link:
                  href: /pricing/
                  text: 詳しく見る
                  data_ga_name: pricing
                  data_ga_location: premium tier
                  aria_label: Premiumプラン
              - id: ultimate
                title: Ultimate
                items:
                  - Premiumの機能に加え、以下の機能が含まれます
                  - 包括的なセキュリティスキャナー(SAST、DAST、秘密、Dependency Scanning、コンテナ、IaC、API、クラスターイメージ、ファジングテストなど)
                  - MRパイプライン内での実行可能な結果
                  - コンプライアンスパイプライン
                  - セキュリティとコンプライアンスのダッシュボード
                  - その他
                link:
                  href: /pricing/
                  text: 詳しく見る
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: Ultimateプラン
                cta:
                  href: /free-trial/
                  text: Ultimateを無料で試す
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: Ultimateプラン
        - name: 'by-industry-case-studies'
          id: case-studies
          data:
            title: お客様が実感しているメリット
            charcoal_bg: true
            rows:
              - title: HackerOne
                subtitle: HackerOne社は、GitLabのインテグレーション型セキュリティでデプロイ速度を5倍まで高めることに成功しました
                image:
                  url: /nuxt-images/software-faster/hackerone-showcase.png
                  alt: コードが表示されたコンピュータ
                button:
                  href: /customers/hackerone/
                  text: 詳細はこちら
                  data_ga_name: learn more
                  data_ga_location: body
              - title: The Zebra
                subtitle: The Zebra社はモノクロでセキュアなパイプラインを達成しました
                image:
                  url: /nuxt-images/customers/thezebra_header.jpeg
                  alt: 携帯電話で車の写真を撮っている
                button:
                  href: /customers/thezebra/
                  text: 詳細はこちら
                  data_ga_name: learn more
                  data_ga_location: body
              - title: Hilti
                subtitle: Hiltis社はCI/CDと堅牢なセキュリティスキャンを導入して、SDLCを加速しました
                image:
                  url: /nuxt-images/blogimages/hilti_cover_image.jpg
                  alt: 建設中の高層ビル
                button:
                  href: /customers/hilti/
                  text: 詳細はこちら
                  data_ga_name: learn more
                  data_ga_location: body

    - name: group-buttons
      data:
        header:
          text: GitLabが継続的なソフトウェアセキュリティに役立つその他の方法もご確認ください
          link:
            text: その他のソリューションを確認
            href: /solutions/
        buttons:
          - text: デリバリーの自動化
            icon_left: automated-code
            href: /solutions/delivery-automation/
          - text: 継続的インテグレーション
            icon_left: continuous-delivery
            href: /solutions/continuous-integration/
          - text: コンプライアンス
            icon_left: shield-check
            href: /solutions/compliance/
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: webcast
              alt: ウェブキャストアイコン
              variant: marketing
            event_type: 動画
            header: DevSecOps概要デモ
            link_text: 今すぐ視聴
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
            href: https://youtu.be/2mmw3SF7Few
            aos_animation: fade-up
            aos_duration: 400
          - icon:
              name: webcast
              alt: ウェブキャストアイコン
              variant: marketing
            event_type: 動画
            header: CI/CDパイプラインにセキュリティを追加する方法を学ぶ
            link_text: 今すぐ視聴
            image: /nuxt-images/features/resources/resources_webcast.png
            href: https://youtu.be/Fd5DhebtScg
            aos_animation: fade-up
            aos_duration: 600
          - icon:
              name: webcast
              alt: ウェブキャストアイコン
              variant: marketing
            event_type: 動画
            header: GitLabセキュリティダッシュボードを使用して脆弱性とリスクを効率的に管理
            link_text: 今すぐ視聴
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
            href: https://youtu.be/p3qt2z1rQk8
            aos_animation: fade-up
            aos_duration: 800
          - icon:
              name: webcast
              alt: ウェブキャストアイコン
              variant: marketing
            event_type: 動画
            header: アプリケーションの依存関係を管理
            link_text: 今すぐ視聴
            image: /nuxt-images/features/resources/resources_waves.png
            href: https://youtu.be/scNS4UuPvLI
            aos_animation: fade-up
            aos_duration: 1000