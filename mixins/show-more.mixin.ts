import Vue from 'vue';

// eslint-disable-next-line @gitlab/no-runtime-template-compiler
export const ShowMoreMixin = Vue.extend({
  data() {
    return {
      numberOfItemsShown: 6, // this can be overwritten in the file that is referencing this mixin
    };
  },
  methods: {
    incrementItemsShown(numberOfTotalItems: number, incrementBy: number) {
      const safeToAdd =
        this.numberOfItemsShown < numberOfTotalItems + incrementBy;

      if (safeToAdd) {
        this.numberOfItemsShown += incrementBy;
      } else {
        this.numberOfItemsShown = numberOfTotalItems;
      }
    },
  },
});
