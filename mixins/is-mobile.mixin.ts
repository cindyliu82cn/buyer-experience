import Vue from 'vue';

// eslint-disable-next-line @gitlab/no-runtime-template-compiler
export const IsMobileMixin = Vue.extend({
  data() {
    return {
      isMobile: false,
      width: 0,
    };
  },
  mounted() {
    this.onResize();
    window.addEventListener('resize', this.onResize);
  },
  destroyed() {
    window.removeEventListener('resize', this.onResize);
  },
  methods: {
    onResize() {
      this.isMobile = window.innerWidth < 769;
      this.width = window.innerWidth;
    },
  },
});
