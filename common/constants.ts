export const DEFAULT_META_DESCRIPTION =
  'Learn more from GitLab, The One DevOps Platform for software innovation.';
export const TWITTER_CREATOR_CONTENT = '@Gitlab';
export const TWITTER_SITE_CONTENT = '@Gitlab';
export const TWITTER_CARD_CONTENT = 'summary_large_image';
export const DEFAULT_OPENGRAPH_IMAGE =
  '/nuxt-images/open-graph/open-graph-gitlab.png';
export const SITE_URL = 'https://about.gitlab.com';

export enum META_NAME {
  description = 'description',
  ogDescription = 'og:description',
  twitterDescription = 'twitter:description',
  twitterTitle = 'twitter:title',
  twitterAltImage = 'twitter:image:alt',
  twitterImage = 'twitter:image',
  twitterCreator = 'twitter:creator',
  twitterCard = 'twitter:card',
  twitterSite = 'twitter:site',
  ogImage = 'og:image',
  ogImageAlt = 'og:image:alt',
  ogTitle = 'og:title',
  article = 'article',
  website = 'website',
  ogType = 'og:type',
  ogUrl = 'og:url',
}

export const RESOURCE_ICON_PATH = Object.freeze({
  case_study: 'case-study',
  article: 'articles',
  book: 'book',
  blog: 'blog',
  podcast: 'podcast-alt',
  report: 'report',
  video: 'video',
  webcast: 'webcast',
  whitepaper: 'whitepapers',
});

export enum FEATURES_HOSTING {
  saas = 'SaaS',
  self_managed = 'Self-Managed',
}

export enum TEMPLATES {
  Default = 'default',
  Education = 'education',
  Industry = 'industry',
  IndustryAlternative = 'industry-alt',
  Feature = 'feature',
}

export const DEFAULT_SCHEMA_ORG = {
  '@context': 'https://schema.org',
  '@type': 'Corporation',
  name: 'GitLab',
  legalName: 'GitLab Inc.',
  tickerSymbol: 'GTLB',
  url: 'https://about.gitlab.com',
  logo: 'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png',
  description:
    'GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form our culture.',
  foundingDate: '2011',
  founders: [
    { '@type': 'Person', name: 'Sid Sijbrandij' },
    { '@type': 'Person', name: 'Dmitriy Zaporozhets' },
  ],
  slogan:
    'Our mission is to change all creative work from read-only to read-write so that everyone can contribute.',
  address: {
    '@type': 'PostalAddress',
    streetAddress: '268 Bush Street #350',
    addressLocality: 'San Francisco',
    addressRegion: 'CA',
    postalCode: '94104',
    addressCountry: 'USA',
  },
  awards:
    "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing - Challenger, DevOps Dozen award for the Best DevOps Solution Provider for 2019, 451 Firestarter Award from 451 Research",
  knowsAbout: [
    { '@type': 'Thing', name: 'DevOps' },
    { '@type': 'Thing', name: 'CI/CD' },
    {
      '@type': 'Thing',
      name: 'DevSecOps',
    },
    { '@type': 'Thing', name: 'GitOps' },
    { '@type': 'Thing', name: 'DevOps Platform' },
  ],
  sameAs: [
    'https://www.facebook.com/gitlab',
    'https://twitter.com/gitlab',
    'https://www.linkedin.com/company/gitlab-com',
    'https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg',
  ],
};

export const SEARCH_TYPE = Object.freeze({
  MARKETING: 'marketing',
  HANDBOOK: 'handbook',
  BLOG: 'blog',
});

export const SEARCH_SORT_OPTIONS = Object.freeze([
  {
    label: 'Suggested',
    value: '_score',
    queryValue: 'suggested',
  },
  {
    label: 'Most popular',
    value: 'popularity',
    queryValue: 'most_popular',
  },
  {
    label: 'Last updated',
    value: 'updated_at',
    queryValue: 'last_updated',
  },
]);

export const LANG_PATH = Object.freeze({
  GERMAN: '/de-de/',
  JAPANESE: '/ja-jp/',
  FRENCH: '/fr-fr/',
});

export const LANG_OPTIONS = Object.freeze([
  {
    label: 'English',
    code: 'en',
    value: 'en-us',
    path: '', // In our site the default language is English and does not have a prefix path
    default: true,
    langLabel: 'Language',
  },
  {
    label: 'Français',
    code: 'fr',
    value: 'fr-fr',
    path: '/fr-fr',
    langLabel: 'Langue',
  },
  {
    label: 'Deutsch',
    code: 'de',
    value: 'de-de',
    path: '/de-de',
    langLabel: 'Sprache',
  },
  {
    label: '日本語',
    code: 'ja',
    value: 'ja-jp',
    path: '/ja-jp',
    langLabel: '言語',
  },
]);

export const CODE_HIGHLIGHTING = Object.freeze({
  javascript: {
    keywords: [
      'var',
      'let',
      'const',
      'if',
      'else',
      'for',
      'while',
      'function',
      'return',
      'new',
      'this',
      'import',
    ],
  },
  python: {
    keywords: ['import', 'def', 'for', 'if', 'else', 'while', 'return'],
  },
  go: {
    keywords: ['import', 'for', 'if', 'else', 'while', 'return', 'func'],
  },
  operators: [
    '=',
    '+',
    '-',
    '*',
    '/',
    '==',
    '===',
    '!=',
    '!==',
    '>',
    '<',
    '>=',
    '<=',
    ':=',
  ],
  brackets: ['{', '}', '(', ')', '[', ']'],
});

export const COMPONENT_NAMES = Object.freeze({
  APPLICATION_FORM: 'application-form',
  BENEFITS: 'benefits',
  BLURB_GRID: 'blurb-grid',
  BY_INDUSTRY_CASE_STUDIES: 'by-industry-case-studies',
  BY_INDUSTRY_INTRO: 'by-industry-intro',
  BY_INDUSTRY_QUOTES_CAROUSEL: 'by-industry-quotes-carousel',
  BY_INDUSTRY_SOLUTIONS_BLOCK: 'by-industry-solutions-block',
  BY_SOLUTION_BENEFITS: 'by-solution-benefits',
  BY_SOLUTION_INTRO: 'by-solution-intro',
  BY_SOLUTION_LINK: 'by-solution-link',
  BY_SOLUTION_LIST: 'solutions-by-solution-list',
  BY_SOLUTION_SHOWCASE: 'by-solution-showcase',
  BY_SOLUTION_VALUE_PROP: 'by-solution-value-prop',
  BY_SOLUTION_EDUCATION_TIERS: 'education-feature-tiers',
  BY_SOLUTION_EDUCATION_STATS: 'education-stats',
  BY_SOLUTION_EDUCATION_CAROUSEL: 'education-case-study-carousel',
  BY_SOLUTION_EDUCATION_TABS: 'case-study-tabs',
  CARD_BASIC_STYLE: 'card-basic-style',
  COPY: 'copy',
  COPY_FORM: 'copy-form',
  COPY_MEDIA: 'copy-media',
  CUSTOMER_LOGOS: 'customer-logos',
  CTA_BLOCK: 'cta-block',
  DEMO_CTA_CARD: 'demo-cta-card',
  DEMO_CTA: 'demo-cta',
  FAQ: 'faq',
  FEATURED_MEDIA: 'featured-media',
  FEATURE_SHOWCASE: 'feature-showcase',
  GET_STARTED_HERO: 'get-started-hero',
  GET_STARTED_MENU: 'get-started-menu',
  GET_STARTED_RESOURCES: 'get-started-resources',
  GET_STARTED_STEPS: 'get-started-steps',
  GET_STARTED_COPY_INFO: 'get-started-copy-info',
  GET_STARTED_NEXT_STEPS: 'get-started-next-steps',
  GROUP_BUTTONS: 'group-buttons',
  HERO_SCROLL_GALLERY: 'hero-scroll-gallery',
  SOLUTIONS_HERO: 'solutions-hero',
  HOME_SOLUTIONS_CONTAINER: 'home-solutions-container',
  INTRO_BLOCK: 'intro-block',
  LOGO_LINKS: 'logo-links',
  NEXT_STEP: 'next-step',
  NEW_QUOTES_CAROUSEL: 'new-quotes-carousel',
  NONPROFIT_OVERVIEW: 'nonprofit-overview',
  NONPROFIT_INTRO: 'nonprofit-intro',
  OVERVIEW_BLOCKS: 'overview-blocks',
  OVERVIEW_CARDS: 'overview-cards',
  ON_DEMAND_DEMOS: 'on-demand-demos',
  OPEN_SOURCE_FORM_SECTION: 'open-source-form-section',
  OPEN_SOURCE_BLOG: 'open-source-blog',
  OPEN_SOURCE_PARTNERS_GRID: 'open-source-partners-grid',
  OPEN_SOURCE_PARTNERS_GRID_FULL: 'open-source-partners-grid-full',
  PULL_QUOTE: 'pull-quote',
  PRICING_TIERS: 'pricing-tiers',
  UPCOMING_DEMO_SERIES: 'upcoming-demo-series',
  REPORT_CTA: 'report-cta',
  RECOGNITION_SPOTLIGHT: 'recognition-spotlight',
  RESOURCE_CAROUSEL: 'resource-carousel',
  SIDE_NAVIGATION_VARIANT: 'side-navigation-variant',
  SINGLE_CTA_BLOCK: 'single-cta-block',
  SLP_SIDE_NAVIGATION: 'slp-side-navigation',
  SOLUTIONS_CARDS: 'solutions-cards',
  SOLUTIONS_CTA_CARD: 'solutions-cta-card',
  SOLUTIONS_FEATURE_LIST: 'solutions-feature-list',
  SOLUTIONS_RESOURCE_CARDS: 'solutions-resource-cards',
  SOLUTIONS_VIDEO_FEATURE: 'solutions-video-feature',
  STARTUPS_INTRO: 'startups-intro',
  STARTUPS_LINK: 'startups-link',
  STARTUPS_OVERVIEW: 'startups-overview',
  STATS_TIMELINE: 'stats-timeline',
  TABBED_FEATURES: 'tabbed-features',
  TABBED_CASE_STUDIES: 'tabbed-case-studies',
  TIER_BLOCK: 'tier-block',
  VIDEOS_CAROUSEL: 'solutions-carousel-videos',
  QUOTES_CAROUSEL: 'quotes-carousel',
  MICROSITE_CARD_BLOCK: 'microsite-card-block',
  MICROSITE_SHOWCASE_CARD: 'microsite-showcase-card',
  MICROSITE_VIDEO: 'microsite-video',
  MICROSITE_TEXT_BLOCK: 'microsite-text-block',
});

export const ICON_VARIANTS = Object.freeze({
  MARKETING: 'marketing',
  PRODUCT: 'product',
});
