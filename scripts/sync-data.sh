#!/bin/sh

LOCAL_DIR="content/synced"

mkdir -p "$LOCAL_DIR"

# Download the data files from the public handbook repo
data_files="\
features.yml \
stages.yml \
categories.yml"

for file in $data_files; do
	echo "fetching $file ..."
	curl -o "$LOCAL_DIR/$file" "https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/${file}"
done
