import { writeFileSync } from 'fs';
import { resolve } from 'path';
import glob from 'glob';

const contentDir = resolve(__dirname, '../content');
const solutionsFiles = glob.GlobSync(`${contentDir}/de-de/solutions/*.yml`);
const getStartedFiles = glob.GlobSync(`${contentDir}/de-de/get-started/*.yml`);
const templatePages = [...solutionsFiles.found, ...getStartedFiles.found];
const localizedPagesNames = templatePages.map((el) =>
  el.slice(el.lastIndexOf('/') + 1).replace('.yml', ''),
);

const outputDir = resolve(__dirname, '../common');
writeFileSync(
  `${outputDir}/localized-pages.ts`,
  `export const localizedPageNames = ${JSON.stringify(localizedPagesNames)};`,
);
