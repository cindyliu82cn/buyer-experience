/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint no-console: 0 */
const fs = require('fs');
const contentful = require('contentful-management');
const process = require('process');
const accessToken = process.argv[2]; // CMA access token
const spaceId = process.argv[3]; // BE space id
const page = process.argv[4];

// Default RegEx to replace
const regexToReplace = /\.html\/$/;

// Output files - txt and yml
const pressReleases = 'outputPressReleases.txt';
const redirects = 'pressReleasesRedirects.yml';

/**
 * Updates the custom page slugs in a Contentful space for a given regex to find and remove.
 * @param {Object} auth - Authentication details.
 * @param {string} auth.accessToken - CMA access token.
 * @param {string} auth.spaceId - Contentful space ID.
 * @param {string} auth.env - Contentful environment.
 * @param {number} [page=1] - Page number for pagination.
 */
function updateCustomPageSlug(auth, page = 1, regexToReplace = regexToReplace) {
  const client = contentful.createClient({
    accessToken: auth.accessToken,
  });

  client
    .getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env))
    .then((environment) => {
      environment
        .getEntries({
          // limit: 3,
          limit: 800,
          skip: (page - 1) * 800,
          // We only want to fetch customPages
          content_type: 'customPage',
        })
        .then(async ({ items }) => {
          console.log(items.length);
          for (const entry of items) {
            await delay(150);

            // We test for the entry having slug, having been published and having the regex we want to replace
            if (
              entry.isPublished() &&
              entry.fields.slug &&
              regexToReplace.test(entry.fields.slug['en-US'])
            ) {
              const logMessage = entry.fields.slug['en-US'];

              // Write into a text file all url instances having the regex we want to replace within the url
              fs.appendFileSync(pressReleases, logMessage + '\n', 'utf8');

              // Remove regex from url
              const transformedSlug = replaceRegex(entry.fields.slug['en-US']);

              // Log to redirects yml file
              const yamlContent = `- sources:\n${[entry.fields.slug['en-US']]
                .map((url) => `\t\t- ${url}\n\t\t- ${url}index.html/\n`)
                .join('')}  target: ${transformedSlug}\n  comp_op: "="\n`;

              fs.appendFileSync(redirects, yamlContent, 'utf8');

              // Transform entry with new value
              entry.fields.slug = {
                'en-US': transformedSlug,
              };

              // Update entry with new slug
              // const updatedEntry = await entry.update();
              // console.log('entry updated: ', updatedEntry);

              // Publish the updated entry
              // updatedEntry
              //   .publish()
              //   .then(() => console.log('entry updated and published'));
              await delay(150);
            }
          }
        });
    });
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function replaceRegex(string) {
  return string.toLowerCase().trim().replace(regexToReplace, '/');
}

updateCustomPageSlug(
  {
    accessToken,
    spaceId,
    env: 'master',
  },
  page,
);
